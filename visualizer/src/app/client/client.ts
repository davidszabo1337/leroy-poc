import {Injectable, NgZone} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {MetadataDto} from '../dto/MetadataDto';
import {Observable, of} from 'rxjs';
import {ApplyMetadataResponseDto} from '../dto/ApplyMetadataResponseDto';
import {catchError} from 'rxjs/operators';
import {MeshData} from '../engine/MeshData';
import * as pako from 'pako';

@Injectable({providedIn: 'root'})
export class RestClient {
  public constructor(private ngZone: NgZone, private http: HttpClient) {
  }
  private url = 'http://localhost:8080/leroy/design-service/api/v1/metadata/apply';

  private httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json; charset=utf-8' })
  };

  public getInsole(metadata: MetadataDto): Observable<ApplyMetadataResponseDto> {
    return this.http.post<ApplyMetadataResponseDto>(this.url, metadata, this.httpOptions).pipe(
      catchError(this.handleError<ApplyMetadataResponseDto>('getInsole'))
    );
  }

  public processResponse(response: ApplyMetadataResponseDto): MeshData[]{
    const meshDatas: MeshData[] = [];
    for (const insoleData of response.insoleData) {
      meshDatas.push(this.getMeshDataFromResponse(insoleData));
    }
    return meshDatas;
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  private log(message: string) {
    console.log(`RestClient: ${message}`);
  }

  private getMeshDataFromResponse(meshData: string): MeshData {
    const fromBase64 = Uint8Array.from(atob(meshData), c => c.charCodeAt(0));
    const restored = JSON.parse(pako.inflate(fromBase64, { to: 'string' }));
    const data = restored as MeshData;
    return data;
  }
}
