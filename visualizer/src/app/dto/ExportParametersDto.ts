import {Vec2Dto} from "./Vec2Dto";


export class ExportParametersDto {
    public embossedText: string;
    public fileName: string;
    public exportFormat: string;
    public fontSize: number;
    public hasPlantarPattern: boolean;
    public numVertices: Vec2Dto;

}