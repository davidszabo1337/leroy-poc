
export class ExportDto {
    public fileId: string;
    public fileName: string;
    public downloadUrl: string;
    public insole: string;
}
