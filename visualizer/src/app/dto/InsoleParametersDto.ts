
export class InsoleParametersDto {
    public adjustArchHeight: number;
    public adjustArchApexPosition: number;
    public forefootPostingAngle: number;
    public shellThickness: number;
    public firstRayCutout: boolean;
    public bevelShellEdgeThickness: number;
    public bevelShellEdgeDistance: number;
    public lengthWidthLength: number;
    public lengthWidthWidth: number;
    public heelPostingAlpha: number;
    public heelPostingWidth: number;
    public heelPostingGamma: number;
    public heelPostingDelta: number;
    public heelPostingEnabled: boolean;
    public regionBasedWidthProximalPoint: number;
    public regionBasedWidthDistalPoint: number;
    public regionBasedWidthWidth: number;
    public regionBasedWidthSmoothing: number;
    public medialFlangeEnabled: boolean;
    public medialFlangeHeight: number;
    public medialFlangeWidth: number;
    public medialFlangeThickness: number;
    public lateralFlangeEnabled: boolean;
    public lateralFlangeHeight: number;
    public lateralFlangeWidth: number;
    public lateralFlangeThickness: number;
    public heelSeatHeight: number;
    public heelSkiveEnabled: boolean;
    public heelSkiveIsMedial: boolean;
    public heelSkiveDepth: number;
    public heelSkiveAngle: number;
    public heelSkiveTilt: number;
}