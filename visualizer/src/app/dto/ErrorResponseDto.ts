
export class ErrorResponseDto {
    public code: number;
    public message: string;
}