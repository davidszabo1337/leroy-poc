import {FootScanParametersDto} from "./FootScanParametersDto";
import {InsoleParametersDto} from "./InsoleParametersDto";
import {ExportParametersDto} from "./ExportParametersDto";


export class MetadataDto {
    public measurementId: string;
    public side: string;
    public footScanParameters: FootScanParametersDto;
    public insoleName?: string;
    public insoleParameters?: InsoleParametersDto;
    public exportParameters?: ExportParametersDto;
}