import {Vec3Dto} from "./Vec3Dto";
import {QuaternionDto} from "./QuaternionDto";


export class FootScanParametersDto {
    public upDirection: string;
    public forwardDirection: string;
    public rightDirection: string;
    public length?: number;
    public width?: number;
    public archHeight?: number;
    public farthestPointLocal?: Vec3Dto;
    public pcaAxisLocal?: Vec3Dto;
    public mpj1?: Vec3Dto;
    public mpj5?: Vec3Dto;
    public boh?: Vec3Dto;
    public mh?: Vec3Dto;
    public lh?: Vec3Dto;
    public arch?: Vec3Dto;
    public position?: Vec3Dto;
    public rotation?: QuaternionDto;
}
