
export class Vec4Dto {
    public x:number;
    public y:number;
    public z:number;
    public w:number;
}
