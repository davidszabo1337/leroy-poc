
export class QuaternionDto {
    public x:number;
    public y:number;
    public z:number;
    public w:number;
}
