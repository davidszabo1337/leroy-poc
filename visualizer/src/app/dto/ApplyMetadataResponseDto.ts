import {MetadataDto} from "./MetadataDto";

export class ApplyMetadataResponseDto {
    public insoleData: string[];
    public metaData: MetadataDto;
}