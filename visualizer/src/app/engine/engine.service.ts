import * as THREE from 'three';
import {ElementRef, Injectable, NgZone, OnDestroy} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { MetadataDto } from '../dto/MetadataDto';
import { ApplyMetadataResponseDto } from '../dto/ApplyMetadataResponseDto';
import * as pako from 'pako';
import { OrbitControls } from '../jsm/controls/OrbitControls.js';
import {Vector3} from 'three';
import {MeshData} from './MeshData';
import {Vertex} from './Vertex';
import {RestClient} from '../client/client';

@Injectable({providedIn: 'root'})
export class EngineService implements OnDestroy {

  public constructor(private ngZone: NgZone) {
  }
  private canvas: HTMLCanvasElement;
  private renderer: THREE.WebGLRenderer;
  private camera: THREE.PerspectiveCamera;
  private scene: THREE.Scene;
  private light: THREE.DirectionalLight;
  private material: THREE.MeshLambertMaterial;
  private meshes: THREE.Mesh[] = [];
  private frameId: number = null;
  private positionNumComponents = 3;
  private normalNumComponents = 3;

  private controls: OrbitControls;

  private static toArray(vec: Vector3): number[] {
      return [vec.x, vec.y, vec.z];
    }

  public ngOnDestroy(): void {
    if (this.frameId != null) {
      cancelAnimationFrame(this.frameId);
    }
  }

  public createScene(canvas: ElementRef<HTMLCanvasElement>): void {
    // The first step is to get the reference of the canvas element from our HTML document
    this.canvas = canvas.nativeElement;

    this.renderer = new THREE.WebGLRenderer({
      canvas: this.canvas,
      alpha: true,    // transparent background
      antialias: true // smooth edges
    });
    this.renderer.setSize(window.innerWidth, window.innerHeight);
    this.scene = new THREE.Scene();
    this.camera = new THREE.PerspectiveCamera(
      75, window.innerWidth / window.innerHeight, 0.1, 1000
    );

    this.scene.background = new THREE.Color (0xffffff);
    this.camera.position.z = 5;
    this.scene.add(this.camera);

    this.controls = new OrbitControls( this.camera, this.renderer.domElement );
    this.controls.update();
    this.light = new THREE.DirectionalLight( 0xffffff);
    this.scene.add(this.light);

    this.material = new THREE.MeshLambertMaterial({color: 0xedeae4});
  }

  public animate(): void {
    // We have to run this outside angular zones,
    // because it could trigger heavy changeDetection cycles.
    this.ngZone.runOutsideAngular(() => {
      if (document.readyState !== 'loading') {
        this.render();
      } else {
        window.addEventListener('DOMContentLoaded', () => {
          this.render();
        });
      }

      window.addEventListener('resize', () => {
        this.resize();
      });
    });
  }

  public render(): void {
    this.frameId = requestAnimationFrame(() => {
      this.render();
    });

    this.renderer.render(this.scene, this.camera);
    this.controls.update();
  }

  public resize(): void {
    const width = window.innerWidth;
    const height = window.innerHeight;

    this.camera.aspect = width / height;
    this.camera.updateProjectionMatrix();

    this.renderer.setSize(width, height);
  }

  public processMeshData(meshDatas: MeshData[]) {
    for (const meshData of meshDatas) {
      const numVertices = meshData.vertexData.length;
      const positions = new Float32Array(numVertices * this.positionNumComponents);
      const normals = new Float32Array(numVertices * this.normalNumComponents);
      let posNdx = 0;
      let nrmNdx = 0;
      for (const vertex of meshData.vertexData) {
        positions.set(EngineService.toArray(vertex.pos), posNdx);
        normals.set(EngineService.toArray(vertex.normal), nrmNdx);
        posNdx += this.positionNumComponents;
        nrmNdx += this.normalNumComponents;
      }
      const geometry = new THREE.BufferGeometry();
      geometry.setAttribute(
        'position',
        new THREE.BufferAttribute(positions, this.positionNumComponents));
      geometry.setAttribute(
        'normal',
        new THREE.BufferAttribute(normals, this.normalNumComponents));
      geometry.setIndex(meshData.indices);
      const mesh = new THREE.Mesh(geometry, new THREE.MeshLambertMaterial({color: 0xedeae4}));
      this.meshes.push(mesh);
    }
    this.addMeshesToScene();
  }

  private addMeshesToScene(): void {
    for (const mesh of this.meshes) {
      this.scene.add(mesh);
    }
  }
}
