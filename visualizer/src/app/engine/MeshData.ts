import {Vertex} from './Vertex';

export class MeshData {
  public vertexData: Vertex[];
  public indices: number[];
}
