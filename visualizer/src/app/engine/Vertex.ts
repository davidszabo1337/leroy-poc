import {Vector3} from 'three';

export class Vertex {
  public pos: Vector3;
  public normal: Vector3;
}
