import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { FootScanParametersDto } from '../dto/FootScanParametersDto';
import { MetadataDto } from '../dto/MetadataDto';
import {EngineService} from './engine.service';
import {RestClient} from '../client/client';


@Component({
  selector: 'app-engine',
  templateUrl: './engine.component.html'
})
export class EngineComponent implements OnInit {

  @ViewChild('rendererCanvas', {static: true})
  public rendererCanvas: ElementRef<HTMLCanvasElement>;

  public constructor(private engServ: EngineService, private restClient: RestClient) {
  }

  public ngOnInit(): void {
    this.engServ.createScene(this.rendererCanvas);
    this.engServ.animate();
  }

  public getInsole(): void {
    const metadata = new MetadataDto();
    metadata.side = 'Left';
    metadata.measurementId = 'asdasdasdasdasd';
    const footScanParameters = new FootScanParametersDto();
    footScanParameters.upDirection = 'NegZ';
    footScanParameters.forwardDirection = 'Y';
    footScanParameters.rightDirection = 'NegX';
    metadata.footScanParameters = footScanParameters;
    this.restClient.getInsole(metadata).subscribe(response => {
      this.engServ.processMeshData(this.restClient.processResponse(response));
    });
  }
}
