using System.Numerics;

namespace Leroy.Model
{
    public enum FootSide
    {
        Left,
        Right
    }
    
    public enum ImportAxisDirection : int
    {
        X, Y, Z, NegX, NegY, NegZ
    }
    
    public class FootScanParameters
    {
        public ImportAxisDirection upDirection { get; set; }

        public ImportAxisDirection forwardDirection { get; set; }

        public ImportAxisDirection rightDirection { get; set; }

        public float length { get; set; }

        public float width { get; set; }

        public float archHeight { get; set; }

        public Vector3 farthestPointLocal { get; set; }

        public Vector3 pcaAxisLocal { get; set; }

        public Vector3 mpj1 { get; set; }

        public Vector3 mpj5 { get; set; }

        public Vector3 boh { get; set; }

        public Vector3 mh { get; set; }

        public Vector3 lh { get; set; }

        public Vector3 arch { get; set; }
        
        public Vector3 position { get; set; }
        
        public Quaternion rotation { get; set; }
        
        public Vector3 mhAligned { get; set; }

        public FootScanParameters()
        {
            upDirection = ImportAxisDirection.NegZ;
            forwardDirection = ImportAxisDirection.Y;
            rightDirection = ImportAxisDirection.X;
            length = 0;
            width = 0;
            archHeight = 0;
            farthestPointLocal = new Vector3();
            pcaAxisLocal = new Vector3();
            mpj1 = new Vector3();
            mpj5 = new Vector3();
            boh = new Vector3();
            mh = new Vector3();
            lh = new Vector3();
            arch = new Vector3();
            position = new Vector3();
            rotation = new Quaternion();
            mhAligned = new Vector3();
        }
    }
}
