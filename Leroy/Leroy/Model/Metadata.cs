namespace Leroy.Model
{
    public class Metadata
    {
        public string measurementId { get; set; } = "";
        public FootSide side { get; set; }
        public FootScanParameters footScanParameters { get; set; }
        public string insoleName { get; set; } = "";
        public InsoleParameters insoleParameters { get; set; }
        public ExportParameters exportParameters { get; set; }

        public Metadata()
        {
            side = FootSide.Left;
            footScanParameters = new FootScanParameters();
            insoleParameters = new InsoleParameters();
            exportParameters = new ExportParameters();
        }
    }
}
