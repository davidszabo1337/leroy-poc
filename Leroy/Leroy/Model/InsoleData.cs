using System.Collections.Generic;

namespace Leroy.Model
{
    public class InsoleData
    {
        public List<string> insoleData { get; set; } = new List<string>();
       
        public Metadata metaData { get; set; }
    }
}
