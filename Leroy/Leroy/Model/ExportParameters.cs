using System.Numerics;

namespace Leroy.Model
{
    public class ExportParameters
    {
        public enum ExportFormat
        {
            OBJ,
            STL
        }
        
        public string embossedText { get; set; }
        
        public string fileName { get; set; }
        
        public ExportFormat exportFormat { get; set; }
        public float fontSize { get; set; }
        public bool hasPlantarPattern { get; set; }
        public Vector2 numVertices { get; set; }

        public ExportParameters()
        {
            embossedText = "";
            fileName = "";
            exportFormat = ExportFormat.STL;
            fontSize = 1.2f;
            hasPlantarPattern = true;
            numVertices = new Vector2(128, 64);
        }
    }
}
