namespace Leroy.Model
{
    public class InsoleParameters
    {
        // arching tool
        public float adjustArchHeight{ get; set; }
        public float adjustArchApexPosition{ get; set; }

        // forefoot posting tool
        public float forefootPostingAngle{ get; set; }

        // shell thickness tool
        public float shellThickness{ get; set; }

        // first ray cutout tool
        public bool firstRayCutout{ get; set; }

        // bevel shell edge tool
        public float bevelShellEdgeThickness{ get; set; }
        public float bevelShellEdgeDistance{ get; set; }

        // length width tool
        public float lengthWidthLength{ get; set; }
        public float lengthWidthWidth{ get; set; }

        // heel posting tool
        public float heelPostingAlpha{ get; set; }
        public float heelPostingWidth{ get; set; }
        public float heelPostingGamma{ get; set; }
        public float heelPostingDelta{ get; set; }
        public bool heelPostingEnabled{ get; set; }

        // region based width tool
        public float regionBasedWidthProximalPoint{ get; set; }
        public float regionBasedWidthDistalPoint{ get; set; }
        public float regionBasedWidthWidth{ get; set; }
        public float regionBasedWidthSmoothing{ get; set; }

        // medial flange tool
        public bool medialFlangeEnabled{ get; set; }
        public float medialFlangeHeight{ get; set; }
        public float medialFlangeWidth{ get; set; }
        public float medialFlangeThickness{ get; set; }

        // lateral flange tool
        public bool lateralFlangeEnabled{ get; set; }
        public float lateralFlangeHeight{ get; set; }
        public float lateralFlangeWidth{ get; set; }
        public float lateralFlangeThickness{ get; set; }

        // heel seat tool
        public float heelSeatHeight{ get; set; }

        // heel skive tool
        public bool heelSkiveEnabled{ get; set; }
        public bool heelSkiveIsMedial{ get; set; }
        public float heelSkiveDepth{ get; set; }
        public float heelSkiveAngle{ get; set; }
        public float heelSkiveTilt{ get; set; }

        public InsoleParameters()
        {
            adjustArchHeight = 0.0f;
            adjustArchApexPosition = 0.0f;
            forefootPostingAngle = 0.0f;
            shellThickness = 0.3f;
            firstRayCutout = false;
            bevelShellEdgeThickness = 0.0f;
            bevelShellEdgeDistance = 1.15f;
            lengthWidthLength = 1.0f;
            lengthWidthWidth = 1.0f;
            heelPostingAlpha = 0.0f;
            heelPostingWidth = 0.0f;
            heelPostingGamma = 15.0f;
            heelPostingDelta = 0.0f;
            heelPostingEnabled = false;
            regionBasedWidthProximalPoint = -2.0f;
            regionBasedWidthDistalPoint = 2.0f;
            regionBasedWidthWidth = 0.0f;
            regionBasedWidthSmoothing = 1.0f;
            medialFlangeEnabled = false;
            medialFlangeHeight = 1.27f;
            medialFlangeWidth = 0.635f;
            medialFlangeThickness = 0.2f;
            lateralFlangeEnabled = false;
            lateralFlangeHeight = 0.635f;
            lateralFlangeWidth = 0.3175f;
            lateralFlangeThickness = 0.2f;
            heelSeatHeight = 0.0f;
            heelSkiveEnabled = false;
            heelSkiveIsMedial = true;
            heelSkiveDepth = 0.0f;
            heelSkiveAngle = 15.0f;
            heelSkiveTilt = 0.0f;
        }
    }
}
