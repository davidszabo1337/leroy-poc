using System;
using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;

namespace Leroy.Dto
{ 

    [DataContract]
    public class InsoleParametersDto : IEquatable<InsoleParametersDto>
    { 

        [JsonProperty(Required = Required.Always)]
        [DataMember(Name="adjustArchHeight")]
        public float adjustArchHeight { get; set; }

        [JsonProperty(Required = Required.Always)]
        [DataMember(Name="adjustArchApexPosition")]
        public float adjustArchApexPosition { get; set; }

        [JsonProperty(Required = Required.Always)]
        [DataMember(Name="forefootPostingAngle")]
        public float forefootPostingAngle { get; set; }

        [JsonProperty(Required = Required.Always)]
        [DataMember(Name="shellThickness")]
        public float shellThickness { get; set; }

        [JsonProperty(Required = Required.Always)]
        [DataMember(Name="firstRayCutout")]
        public bool firstRayCutout { get; set; }

        [JsonProperty(Required = Required.Always)]
        [DataMember(Name="bevelShellEdgeThickness")]
        public float bevelShellEdgeThickness { get; set; }

        [JsonProperty(Required = Required.Always)]
        [DataMember(Name="bevelShellEdgeDistance")]
        public float bevelShellEdgeDistance { get; set; }

        [JsonProperty(Required = Required.Always)]
        [DataMember(Name="lengthWidthLength")]
        public float lengthWidthLength { get; set; }

        [JsonProperty(Required = Required.Always)]
        [DataMember(Name="lengthWidthWidth")]
        public float lengthWidthWidth { get; set; }

        [JsonProperty(Required = Required.Always)]
        [DataMember(Name="heelPostingAlpha")]
        public float heelPostingAlpha { get; set; }

        [JsonProperty(Required = Required.Always)]
        [DataMember(Name="heelPostingWidth")]
        public float heelPostingWidth { get; set; }

        [JsonProperty(Required = Required.Always)]
        [DataMember(Name="heelPostingGamma")]
        public float heelPostingGamma { get; set; }

        [JsonProperty(Required = Required.Always)]
        [DataMember(Name="heelPostingDelta")]
        public float heelPostingDelta { get; set; }

        [JsonProperty(Required = Required.Always)]
        [DataMember(Name="heelPostingEnabled")]
        public bool heelPostingEnabled { get; set; }

        [JsonProperty(Required = Required.Always)]
        [DataMember(Name="regionBasedWidthProximalPoint")]
        public float regionBasedWidthProximalPoint { get; set; }

        [JsonProperty(Required = Required.Always)]
        [DataMember(Name="regionBasedWidthDistalPoint")]
        public float regionBasedWidthDistalPoint { get; set; }

        [JsonProperty(Required = Required.Always)]
        [DataMember(Name="regionBasedWidthWidth")]
        public float regionBasedWidthWidth { get; set; }

        [JsonProperty(Required = Required.Always)]
        [DataMember(Name="regionBasedWidthSmoothing")]
        public float regionBasedWidthSmoothing { get; set; }

        [JsonProperty(Required = Required.Always)]
        [DataMember(Name="medialFlangeEnabled")]
        public bool medialFlangeEnabled { get; set; }

        [JsonProperty(Required = Required.Always)]
        [DataMember(Name="medialFlangeHeight")]
        public float medialFlangeHeight { get; set; }

        [JsonProperty(Required = Required.Always)]
        [DataMember(Name="medialFlangeWidth")]
        public float medialFlangeWidth { get; set; }

        [JsonProperty(Required = Required.Always)]
        [DataMember(Name="medialFlangeThickness")]
        public float medialFlangeThickness { get; set; }

        [JsonProperty(Required = Required.Always)]
        [DataMember(Name="lateralFlangeEnabled")]
        public bool lateralFlangeEnabled { get; set; }

        [JsonProperty(Required = Required.Always)]
        [DataMember(Name="lateralFlangeHeight")]
        public float lateralFlangeHeight { get; set; }

        [JsonProperty(Required = Required.Always)]
        [DataMember(Name="lateralFlangeWidth")]
        public float lateralFlangeWidth { get; set; }

        [JsonProperty(Required = Required.Always)]
        [DataMember(Name="lateralFlangeThickness")]
        public float lateralFlangeThickness { get; set; }

        [JsonProperty(Required = Required.Always)]
        [DataMember(Name="heelSeatHeight")]
        public float heelSeatHeight { get; set; }

        [JsonProperty(Required = Required.Always)]
        [DataMember(Name="heelSkiveEnabled")]
        public bool heelSkiveEnabled { get; set; }

        [JsonProperty(Required = Required.Always)]
        [DataMember(Name="heelSkiveIsMedial")]
        public bool heelSkiveIsMedial { get; set; }

        [JsonProperty(Required = Required.Always)]
        [DataMember(Name="heelSkiveDepth")]
        public float heelSkiveDepth { get; set; }

        [JsonProperty(Required = Required.Always)]
        [DataMember(Name="heelSkiveAngle")]
        public float heelSkiveAngle { get; set; }

        [JsonProperty(Required = Required.Always)]
        [DataMember(Name="heelSkiveTilt")]
        public float heelSkiveTilt { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class InsoleParameters {\n");
            sb.Append("  AdjustArchHeight: ").Append(adjustArchHeight).Append("\n");
            sb.Append("  AdjustArchApexPosition: ").Append(adjustArchApexPosition).Append("\n");
            sb.Append("  ForefootPostingAngle: ").Append(forefootPostingAngle).Append("\n");
            sb.Append("  ShellThickness: ").Append(shellThickness).Append("\n");
            sb.Append("  FirstRayCutout: ").Append(firstRayCutout).Append("\n");
            sb.Append("  BevelShellEdgeThickness: ").Append(bevelShellEdgeThickness).Append("\n");
            sb.Append("  BevelShellEdgeDistance: ").Append(bevelShellEdgeDistance).Append("\n");
            sb.Append("  LengthWidthLength: ").Append(lengthWidthLength).Append("\n");
            sb.Append("  LengthWidthWidth: ").Append(lengthWidthWidth).Append("\n");
            sb.Append("  HeelPostingAlpha: ").Append(heelPostingAlpha).Append("\n");
            sb.Append("  HeelPostingWidth: ").Append(heelPostingWidth).Append("\n");
            sb.Append("  HeelPostingGamma: ").Append(heelPostingGamma).Append("\n");
            sb.Append("  HeelPostingDelta: ").Append(heelPostingDelta).Append("\n");
            sb.Append("  HeelPostingEnabled: ").Append(heelPostingEnabled).Append("\n");
            sb.Append("  RegionBasedWidthProximalPoint: ").Append(regionBasedWidthProximalPoint).Append("\n");
            sb.Append("  RegionBasedWidthDistalPoint: ").Append(regionBasedWidthDistalPoint).Append("\n");
            sb.Append("  RegionBasedWidthWidth: ").Append(regionBasedWidthWidth).Append("\n");
            sb.Append("  RegionBasedWidthSmoothing: ").Append(regionBasedWidthSmoothing).Append("\n");
            sb.Append("  MedialFlangeEnabled: ").Append(medialFlangeEnabled).Append("\n");
            sb.Append("  MedialFlangeHeight: ").Append(medialFlangeHeight).Append("\n");
            sb.Append("  MedialFlangeWidth: ").Append(medialFlangeWidth).Append("\n");
            sb.Append("  MedialFlangeThickness: ").Append(medialFlangeThickness).Append("\n");
            sb.Append("  LateralFlangeEnabled: ").Append(lateralFlangeEnabled).Append("\n");
            sb.Append("  LateralFlangeHeight: ").Append(lateralFlangeHeight).Append("\n");
            sb.Append("  LateralFlangeWidth: ").Append(lateralFlangeWidth).Append("\n");
            sb.Append("  LateralFlangeThickness: ").Append(lateralFlangeThickness).Append("\n");
            sb.Append("  HeelSeatHeight: ").Append(heelSeatHeight).Append("\n");
            sb.Append("  HeelSkiveEnabled: ").Append(heelSkiveEnabled).Append("\n");
            sb.Append("  HeelSkiveIsMedial: ").Append(heelSkiveIsMedial).Append("\n");
            sb.Append("  HeelSkiveDepth: ").Append(heelSkiveDepth).Append("\n");
            sb.Append("  HeelSkiveAngle: ").Append(heelSkiveAngle).Append("\n");
            sb.Append("  HeelSkiveTilt: ").Append(heelSkiveTilt).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((InsoleParametersDto)obj);
        }

        public bool Equals(InsoleParametersDto other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    adjustArchHeight == other.adjustArchHeight ||
                    adjustArchHeight != null &&
                    adjustArchHeight.Equals(other.adjustArchHeight)
                ) && 
                (
                    adjustArchApexPosition == other.adjustArchApexPosition ||
                    adjustArchApexPosition != null &&
                    adjustArchApexPosition.Equals(other.adjustArchApexPosition)
                ) && 
                (
                    forefootPostingAngle == other.forefootPostingAngle ||
                    forefootPostingAngle != null &&
                    forefootPostingAngle.Equals(other.forefootPostingAngle)
                ) && 
                (
                    shellThickness == other.shellThickness ||
                    shellThickness != null &&
                    shellThickness.Equals(other.shellThickness)
                ) && 
                (
                    firstRayCutout == other.firstRayCutout ||
                    firstRayCutout != null &&
                    firstRayCutout.Equals(other.firstRayCutout)
                ) && 
                (
                    bevelShellEdgeThickness == other.bevelShellEdgeThickness ||
                    bevelShellEdgeThickness != null &&
                    bevelShellEdgeThickness.Equals(other.bevelShellEdgeThickness)
                ) && 
                (
                    bevelShellEdgeDistance == other.bevelShellEdgeDistance ||
                    bevelShellEdgeDistance != null &&
                    bevelShellEdgeDistance.Equals(other.bevelShellEdgeDistance)
                ) && 
                (
                    lengthWidthLength == other.lengthWidthLength ||
                    lengthWidthLength != null &&
                    lengthWidthLength.Equals(other.lengthWidthLength)
                ) && 
                (
                    lengthWidthWidth == other.lengthWidthWidth ||
                    lengthWidthWidth != null &&
                    lengthWidthWidth.Equals(other.lengthWidthWidth)
                ) && 
                (
                    heelPostingAlpha == other.heelPostingAlpha ||
                    heelPostingAlpha != null &&
                    heelPostingAlpha.Equals(other.heelPostingAlpha)
                ) && 
                (
                    heelPostingWidth == other.heelPostingWidth ||
                    heelPostingWidth != null &&
                    heelPostingWidth.Equals(other.heelPostingWidth)
                ) && 
                (
                    heelPostingGamma == other.heelPostingGamma ||
                    heelPostingGamma != null &&
                    heelPostingGamma.Equals(other.heelPostingGamma)
                ) && 
                (
                    heelPostingDelta == other.heelPostingDelta ||
                    heelPostingDelta != null &&
                    heelPostingDelta.Equals(other.heelPostingDelta)
                ) && 
                (
                    heelPostingEnabled == other.heelPostingEnabled ||
                    heelPostingEnabled != null &&
                    heelPostingEnabled.Equals(other.heelPostingEnabled)
                ) && 
                (
                    regionBasedWidthProximalPoint == other.regionBasedWidthProximalPoint ||
                    regionBasedWidthProximalPoint != null &&
                    regionBasedWidthProximalPoint.Equals(other.regionBasedWidthProximalPoint)
                ) && 
                (
                    regionBasedWidthDistalPoint == other.regionBasedWidthDistalPoint ||
                    regionBasedWidthDistalPoint != null &&
                    regionBasedWidthDistalPoint.Equals(other.regionBasedWidthDistalPoint)
                ) && 
                (
                    regionBasedWidthWidth == other.regionBasedWidthWidth ||
                    regionBasedWidthWidth != null &&
                    regionBasedWidthWidth.Equals(other.regionBasedWidthWidth)
                ) && 
                (
                    regionBasedWidthSmoothing == other.regionBasedWidthSmoothing ||
                    regionBasedWidthSmoothing != null &&
                    regionBasedWidthSmoothing.Equals(other.regionBasedWidthSmoothing)
                ) && 
                (
                    medialFlangeEnabled == other.medialFlangeEnabled ||
                    medialFlangeEnabled != null &&
                    medialFlangeEnabled.Equals(other.medialFlangeEnabled)
                ) && 
                (
                    medialFlangeHeight == other.medialFlangeHeight ||
                    medialFlangeHeight != null &&
                    medialFlangeHeight.Equals(other.medialFlangeHeight)
                ) && 
                (
                    medialFlangeWidth == other.medialFlangeWidth ||
                    medialFlangeWidth != null &&
                    medialFlangeWidth.Equals(other.medialFlangeWidth)
                ) && 
                (
                    medialFlangeThickness == other.medialFlangeThickness ||
                    medialFlangeThickness != null &&
                    medialFlangeThickness.Equals(other.medialFlangeThickness)
                ) && 
                (
                    lateralFlangeEnabled == other.lateralFlangeEnabled ||
                    lateralFlangeEnabled != null &&
                    lateralFlangeEnabled.Equals(other.lateralFlangeEnabled)
                ) && 
                (
                    lateralFlangeHeight == other.lateralFlangeHeight ||
                    lateralFlangeHeight != null &&
                    lateralFlangeHeight.Equals(other.lateralFlangeHeight)
                ) && 
                (
                    lateralFlangeWidth == other.lateralFlangeWidth ||
                    lateralFlangeWidth != null &&
                    lateralFlangeWidth.Equals(other.lateralFlangeWidth)
                ) && 
                (
                    lateralFlangeThickness == other.lateralFlangeThickness ||
                    lateralFlangeThickness != null &&
                    lateralFlangeThickness.Equals(other.lateralFlangeThickness)
                ) && 
                (
                    heelSeatHeight == other.heelSeatHeight ||
                    heelSeatHeight != null &&
                    heelSeatHeight.Equals(other.heelSeatHeight)
                ) && 
                (
                    heelSkiveEnabled == other.heelSkiveEnabled ||
                    heelSkiveEnabled != null &&
                    heelSkiveEnabled.Equals(other.heelSkiveEnabled)
                ) && 
                (
                    heelSkiveIsMedial == other.heelSkiveIsMedial ||
                    heelSkiveIsMedial != null &&
                    heelSkiveIsMedial.Equals(other.heelSkiveIsMedial)
                ) && 
                (
                    heelSkiveDepth == other.heelSkiveDepth ||
                    heelSkiveDepth != null &&
                    heelSkiveDepth.Equals(other.heelSkiveDepth)
                ) && 
                (
                    heelSkiveAngle == other.heelSkiveAngle ||
                    heelSkiveAngle != null &&
                    heelSkiveAngle.Equals(other.heelSkiveAngle)
                ) && 
                (
                    heelSkiveTilt == other.heelSkiveTilt ||
                    heelSkiveTilt != null &&
                    heelSkiveTilt.Equals(other.heelSkiveTilt)
                );
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = 41;
                    if (adjustArchHeight != null)
                    hashCode = hashCode * 59 + adjustArchHeight.GetHashCode();
                    if (adjustArchApexPosition != null)
                    hashCode = hashCode * 59 + adjustArchApexPosition.GetHashCode();
                    if (forefootPostingAngle != null)
                    hashCode = hashCode * 59 + forefootPostingAngle.GetHashCode();
                    if (shellThickness != null)
                    hashCode = hashCode * 59 + shellThickness.GetHashCode();
                    if (firstRayCutout != null)
                    hashCode = hashCode * 59 + firstRayCutout.GetHashCode();
                    if (bevelShellEdgeThickness != null)
                    hashCode = hashCode * 59 + bevelShellEdgeThickness.GetHashCode();
                    if (bevelShellEdgeDistance != null)
                    hashCode = hashCode * 59 + bevelShellEdgeDistance.GetHashCode();
                    if (lengthWidthLength != null)
                    hashCode = hashCode * 59 + lengthWidthLength.GetHashCode();
                    if (lengthWidthWidth != null)
                    hashCode = hashCode * 59 + lengthWidthWidth.GetHashCode();
                    if (heelPostingAlpha != null)
                    hashCode = hashCode * 59 + heelPostingAlpha.GetHashCode();
                    if (heelPostingWidth != null)
                    hashCode = hashCode * 59 + heelPostingWidth.GetHashCode();
                    if (heelPostingGamma != null)
                    hashCode = hashCode * 59 + heelPostingGamma.GetHashCode();
                    if (heelPostingDelta != null)
                    hashCode = hashCode * 59 + heelPostingDelta.GetHashCode();
                    if (heelPostingEnabled != null)
                    hashCode = hashCode * 59 + heelPostingEnabled.GetHashCode();
                    if (regionBasedWidthProximalPoint != null)
                    hashCode = hashCode * 59 + regionBasedWidthProximalPoint.GetHashCode();
                    if (regionBasedWidthDistalPoint != null)
                    hashCode = hashCode * 59 + regionBasedWidthDistalPoint.GetHashCode();
                    if (regionBasedWidthWidth != null)
                    hashCode = hashCode * 59 + regionBasedWidthWidth.GetHashCode();
                    if (regionBasedWidthSmoothing != null)
                    hashCode = hashCode * 59 + regionBasedWidthSmoothing.GetHashCode();
                    if (medialFlangeEnabled != null)
                    hashCode = hashCode * 59 + medialFlangeEnabled.GetHashCode();
                    if (medialFlangeHeight != null)
                    hashCode = hashCode * 59 + medialFlangeHeight.GetHashCode();
                    if (medialFlangeWidth != null)
                    hashCode = hashCode * 59 + medialFlangeWidth.GetHashCode();
                    if (medialFlangeThickness != null)
                    hashCode = hashCode * 59 + medialFlangeThickness.GetHashCode();
                    if (lateralFlangeEnabled != null)
                    hashCode = hashCode * 59 + lateralFlangeEnabled.GetHashCode();
                    if (lateralFlangeHeight != null)
                    hashCode = hashCode * 59 + lateralFlangeHeight.GetHashCode();
                    if (lateralFlangeWidth != null)
                    hashCode = hashCode * 59 + lateralFlangeWidth.GetHashCode();
                    if (lateralFlangeThickness != null)
                    hashCode = hashCode * 59 + lateralFlangeThickness.GetHashCode();
                    if (heelSeatHeight != null)
                    hashCode = hashCode * 59 + heelSeatHeight.GetHashCode();
                    if (heelSkiveEnabled != null)
                    hashCode = hashCode * 59 + heelSkiveEnabled.GetHashCode();
                    if (heelSkiveIsMedial != null)
                    hashCode = hashCode * 59 + heelSkiveIsMedial.GetHashCode();
                    if (heelSkiveDepth != null)
                    hashCode = hashCode * 59 + heelSkiveDepth.GetHashCode();
                    if (heelSkiveAngle != null)
                    hashCode = hashCode * 59 + heelSkiveAngle.GetHashCode();
                    if (heelSkiveTilt != null)
                    hashCode = hashCode * 59 + heelSkiveTilt.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(InsoleParametersDto left, InsoleParametersDto right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(InsoleParametersDto left, InsoleParametersDto right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
