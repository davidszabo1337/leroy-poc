using System;
using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;

namespace Leroy.Dto
{

    [DataContract]
    public class Vec3Dto : IEquatable<Vec3Dto>
    {
        public Vec3Dto(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        [DataMember(Name="x")]
        public float x { get; set; }

        [DataMember(Name="y")]
        public float y { get; set; }

        [DataMember(Name="z")]
        public float z { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Vec3 {\n");
            sb.Append("  X: ").Append(x).Append("\n");
            sb.Append("  Y: ").Append(y).Append("\n");
            sb.Append("  Z: ").Append(z).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((Vec3Dto)obj);
        }

        public bool Equals(Vec3Dto other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    x == other.x ||
                    x != null &&
                    x.Equals(other.x)
                ) && 
                (
                    y == other.y ||
                    y != null &&
                    y.Equals(other.y)
                ) && 
                (
                    z == other.z ||
                    z != null &&
                    z.Equals(other.z)
                );
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = 41;
                    if (x != null)
                    hashCode = hashCode * 59 + x.GetHashCode();
                    if (y != null)
                    hashCode = hashCode * 59 + y.GetHashCode();
                    if (z != null)
                    hashCode = hashCode * 59 + z.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(Vec3Dto left, Vec3Dto right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Vec3Dto left, Vec3Dto right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
