using System;
using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;

namespace Leroy.Dto
{

    [DataContract]
    public class Vec4Dto : IEquatable<Vec4Dto>
    {
        public Vec4Dto(float x, float y, float z, float w)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.w = w;
        }

        [DataMember(Name="x")]
        public float x { get; set; }

        [DataMember(Name="y")]
        public float y { get; set; }

        [DataMember(Name="z")]
        public float z { get; set; }

        [DataMember(Name="w")]
        public float w { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Vec4 {\n");
            sb.Append("  X: ").Append(x).Append("\n");
            sb.Append("  Y: ").Append(y).Append("\n");
            sb.Append("  Z: ").Append(z).Append("\n");
            sb.Append("  W: ").Append(w).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((Vec4Dto)obj);
        }

        public bool Equals(Vec4Dto other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    x == other.x ||
                    x != null &&
                    x.Equals(other.x)
                ) && 
                (
                    y == other.y ||
                    y != null &&
                    y.Equals(other.y)
                ) && 
                (
                    z == other.z ||
                    z != null &&
                    z.Equals(other.z)
                ) && 
                (
                    w == other.w ||
                    w != null &&
                    w.Equals(other.w)
                );
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = 41;
                    if (x != null)
                    hashCode = hashCode * 59 + x.GetHashCode();
                    if (y != null)
                    hashCode = hashCode * 59 + y.GetHashCode();
                    if (z != null)
                    hashCode = hashCode * 59 + z.GetHashCode();
                    if (w != null)
                    hashCode = hashCode * 59 + w.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(Vec4Dto left, Vec4Dto right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Vec4Dto left, Vec4Dto right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
