using System;
using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;

namespace Leroy.Dto
{
    [DataContract]
    public class FootScanParametersDto : IEquatable<FootScanParametersDto>
    {

        [DataMember(Name = "upDirection")]
        public string upDirection { get; set; }

        [DataMember(Name = "forwardDirection")]
        public string forwardDirection { get; set; }

        [DataMember(Name = "rightDirection")]
        public string rightDirection { get; set; }

        [DataMember(Name = "length")] 
        public float length { get; set; }

        [DataMember(Name = "width")] 
        public float width { get; set; }

        [DataMember(Name = "archHeight")] 
        public float archHeight { get; set; }

        [DataMember(Name = "farthestPointLocal")]
        public Vec3Dto farthestPointLocal { get; set; }
        
        [DataMember(Name = "pcaAxisLocal")] 
        public Vec3Dto pcaAxisLocal { get; set; }

        [DataMember(Name = "mpj1")] 
        public Vec3Dto mpj1 { get; set; }

        [DataMember(Name = "mpj5")] 
        public Vec3Dto mpj5 { get; set; }

        [DataMember(Name = "boh")] 
        public Vec3Dto boh { get; set; }

        [DataMember(Name = "mh")] 
        public Vec3Dto mh { get; set; }

        [DataMember(Name = "lh")] 
        public Vec3Dto lh { get; set; }

        [DataMember(Name = "arch")] 
        public Vec3Dto arch { get; set; }

        [DataMember(Name = "position")] public 
        Vec3Dto position { get; set; } = new Vec3Dto(0, 0, 0);

        [DataMember(Name="rotation")]
        public QuaternionDto rotation { get; set; } = new QuaternionDto(0, 0, 0, 1);
        
        [DataMember(Name = "mhAligned")] 
        public Vec3Dto mhAligned { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class FootScanParameters {\n");
            sb.Append("  UpDirection: ").Append(upDirection).Append("\n");
            sb.Append("  ForwardDirection: ").Append(forwardDirection).Append("\n");
            sb.Append("  RightDirection: ").Append(rightDirection).Append("\n");
            sb.Append("  Length: ").Append(length).Append("\n");
            sb.Append("  Width: ").Append(width).Append("\n");
            sb.Append("  ArchHeigth: ").Append(archHeight).Append("\n");
            sb.Append("  FarthestPointLocal: ").Append(farthestPointLocal).Append("\n");
            sb.Append("  PcaAxisLocal: ").Append(pcaAxisLocal).Append("\n");
            sb.Append("  Mpj1: ").Append(mpj1).Append("\n");
            sb.Append("  Mpj5: ").Append(mpj5).Append("\n");
            sb.Append("  Boh: ").Append(boh).Append("\n");
            sb.Append("  Mh: ").Append(mh).Append("\n");
            sb.Append("  Lh: ").Append(lh).Append("\n");
            sb.Append("  Arch: ").Append(arch).Append("\n");
            sb.Append("  Position: ").Append(position).Append("\n");
            sb.Append("  Rotation: ").Append(rotation).Append("\n");
            sb.Append("  Rotation: ").Append(mhAligned).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((FootScanParametersDto)obj);
        }

        public bool Equals(FootScanParametersDto other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    upDirection == other.upDirection ||
                    upDirection != null &&
                    upDirection.Equals(other.upDirection)
                ) && 
                (
                    forwardDirection == other.forwardDirection ||
                    forwardDirection != null &&
                    forwardDirection.Equals(other.forwardDirection)
                ) && 
                (
                    rightDirection == other.rightDirection ||
                    rightDirection != null &&
                    rightDirection.Equals(other.rightDirection)
                ) && 
                (
                    length == other.length ||
                    length != null &&
                    length.Equals(other.length)
                ) && 
                (
                    width == other.width ||
                    width != null &&
                    width.Equals(other.width)
                ) && 
                (
                    archHeight == other.archHeight ||
                    archHeight != null &&
                    archHeight.Equals(other.archHeight)
                ) && 
                (
                    farthestPointLocal == other.farthestPointLocal ||
                    farthestPointLocal != null &&
                    farthestPointLocal.Equals(other.farthestPointLocal)
                ) && 
                (
                    pcaAxisLocal == other.pcaAxisLocal ||
                    pcaAxisLocal != null &&
                    pcaAxisLocal.Equals(other.pcaAxisLocal)
                ) && 
                (
                    mpj1 == other.mpj1 ||
                    mpj1 != null &&
                    mpj1.Equals(other.mpj1)
                ) && 
                (
                    mpj5 == other.mpj5 ||
                    mpj5 != null &&
                    mpj5.Equals(other.mpj5)
                ) && 
                (
                    boh == other.boh ||
                    boh != null &&
                    boh.Equals(other.boh)
                ) && 
                (
                    mh == other.mh ||
                    mh != null &&
                    mh.Equals(other.mh)
                ) && 
                (
                    lh == other.lh ||
                    lh != null &&
                    lh.Equals(other.lh)
                ) && 
                (
                    arch == other.arch ||
                    arch != null &&
                    arch.Equals(other.arch)
                )&& 
                (
                    position == other.position ||
                    position != null &&
                    position.Equals(other.position)
                )
                && 
                (
                    rotation == other.rotation ||
                    rotation != null &&
                    rotation.Equals(other.rotation)
                )&& 
                (
                    mhAligned == other.mhAligned ||
                    mhAligned != null &&
                    mhAligned.Equals(other.mhAligned)
                );
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = 41;
                    if (upDirection != null)
                    hashCode = hashCode * 59 + upDirection.GetHashCode();
                    if (forwardDirection != null)
                    hashCode = hashCode * 59 + forwardDirection.GetHashCode();
                    if (rightDirection != null)
                    hashCode = hashCode * 59 + rightDirection.GetHashCode();
                    if (length != null)
                    hashCode = hashCode * 59 + length.GetHashCode();
                    if (width != null)
                    hashCode = hashCode * 59 + width.GetHashCode();
                    if (archHeight != null)
                    hashCode = hashCode * 59 + archHeight.GetHashCode();
                    if (farthestPointLocal != null)
                    hashCode = hashCode * 59 + farthestPointLocal.GetHashCode();
                    if (pcaAxisLocal != null)
                    hashCode = hashCode * 59 + pcaAxisLocal.GetHashCode();
                    if (mpj1 != null)
                    hashCode = hashCode * 59 + mpj1.GetHashCode();
                    if (mpj5 != null)
                    hashCode = hashCode * 59 + mpj5.GetHashCode();
                    if (boh != null)
                    hashCode = hashCode * 59 + boh.GetHashCode();
                    if (mh != null)
                    hashCode = hashCode * 59 + mh.GetHashCode();
                    if (lh != null)
                    hashCode = hashCode * 59 + lh.GetHashCode();
                    if (arch != null)
                    hashCode = hashCode * 59 + arch.GetHashCode();
                    if (position != null)
                        hashCode = hashCode * 59 + position.GetHashCode();
                    if (rotation != null)
                        hashCode = hashCode * 59 + rotation.GetHashCode();
                    if (mhAligned != null)
                        hashCode = hashCode * 59 + mhAligned.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(FootScanParametersDto left, FootScanParametersDto right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(FootScanParametersDto left, FootScanParametersDto right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
