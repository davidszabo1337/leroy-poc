using System;
using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;

namespace Leroy.Dto
{ 
    [DataContract]
    public class AppVersionDto : IEquatable<AppVersionDto>
    {
        public AppVersionDto(string version)
        {
            this.version = version;
        }

        [DataMember(Name="version")]
        public string version { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class AppVersion {\n");
            sb.Append("  version: ").Append(version).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((AppVersionDto)obj);
        }

        public bool Equals(AppVersionDto other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    version == other.version ||
                    version != null &&
                    version.Equals(other.version)
                );
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = 41;
                    if (version != null)
                    hashCode = hashCode * 59 + version.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(AppVersionDto left, AppVersionDto right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(AppVersionDto left, AppVersionDto right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
