using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;

namespace Leroy.Dto
{

    [DataContract]
    public class MetadataDto : IEquatable<MetadataDto>
    {

        [Required(AllowEmptyStrings = false)]
        [JsonProperty("measurementId", Required = Required.Always)]
        [DataMember(Name = "measurementId")]
        public string measurementId { get; set; }

        [Required]
        [JsonProperty("side", Required = Required.Always)]
        [DataMember(Name = "side")]
        public string side { get; set; }

        [Required]
        [JsonProperty(Required = Required.Always)]
        [DataMember(Name="footScanParameters")]
        public FootScanParametersDto footScanParameters { get; set; }

        [DataMember(Name = "insoleName")]
        public string insoleName { get; set; } = "";

        [DataMember(Name="insoleParameters")]
        public InsoleParametersDto insoleParameters { get; set; }

        [DataMember(Name="exportParameters")]
        public ExportParametersDto exportParameters { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Metadata {\n");
            sb.Append("  measurementId: ").Append(measurementId).Append("\n");
            sb.Append("  side: ").Append(side).Append("\n");
            sb.Append("  footScanParameters: ").Append(footScanParameters).Append("\n");
            sb.Append("  insoleName: ").Append(insoleName).Append("\n");
            sb.Append("  insoleParameters: ").Append(insoleParameters).Append("\n");
            sb.Append("  exportParameters: ").Append(exportParameters).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        public string ToJson()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this, Newtonsoft.Json.Formatting.Indented);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((MetadataDto)obj);
        }

        public bool Equals(MetadataDto other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    measurementId == other.measurementId ||
                    measurementId != null &&
                    measurementId.Equals(other.measurementId)
                ) &&
                (
                    side == other.side ||
                    side != null &&
                    side.Equals(other.side)
                ) &&
                (
                    footScanParameters == other.footScanParameters ||
                    footScanParameters != null &&
                    footScanParameters.Equals(other.footScanParameters)
                ) && 
                (
                    insoleName == other.insoleName ||
                    insoleName != null &&
                    insoleName.Equals(other.insoleName)
                ) && 
                (
                    insoleParameters == other.insoleParameters ||
                    insoleParameters != null &&
                    insoleParameters.Equals(other.insoleParameters)
                ) && 
                (
                    exportParameters == other.exportParameters ||
                    exportParameters != null &&
                    exportParameters.Equals(other.exportParameters)
                );
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = 41;
                if (measurementId != null)
                    hashCode = hashCode * 59 + measurementId.GetHashCode();
                if (side != null)
                    hashCode = hashCode * 59 + side.GetHashCode();
                if (footScanParameters != null)
                    hashCode = hashCode * 59 + footScanParameters.GetHashCode();
                    if (insoleName != null)
                    hashCode = hashCode * 59 + insoleName.GetHashCode();
                    if (insoleParameters != null)
                    hashCode = hashCode * 59 + insoleParameters.GetHashCode();
                    if (exportParameters != null)
                    hashCode = hashCode * 59 + exportParameters.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(MetadataDto left, MetadataDto right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(MetadataDto left, MetadataDto right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
