using System;
using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;

namespace Leroy.Dto
{

    [DataContract]
    public class Vec2Dto : IEquatable<Vec2Dto>
    {
        public Vec2Dto(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        [DataMember(Name="x")]
        public int x { get; set; }

        [DataMember(Name="y")]
        public int y { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Vec2 {\n");
            sb.Append("  X: ").Append(x).Append("\n");
            sb.Append("  Y: ").Append(y).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((Vec2Dto)obj);
        }

        public bool Equals(Vec2Dto other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    x == other.x ||
                    x != null &&
                    x.Equals(other.x)
                ) && 
                (
                    y == other.y ||
                    y != null &&
                    y.Equals(other.y)
                );
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = 41;               
                    if (x != null)
                    hashCode = hashCode * 59 + x.GetHashCode();
                    if (y != null)
                    hashCode = hashCode * 59 + y.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(Vec2Dto left, Vec2Dto right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Vec2Dto left, Vec2Dto right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
