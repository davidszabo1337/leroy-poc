using System;
using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;

namespace Leroy.Dto
{ 
    [DataContract]
    public class ExportParametersDto : IEquatable<ExportParametersDto>
    { 
        [DataMember(Name="embossedText")]
        public string embossedText { get; set; }
        
        [DataMember(Name="fileName")]
        public string fileName { get; set; }

        [DataMember(Name="exportFormat")]
        public string exportFormat { get; set; }
        
        [DataMember(Name="fontSize")]
        public float fontSize { get; set; }

        [DataMember(Name="hasPlantarPattern")]
        public bool hasPlantarPattern { get; set; }

        [DataMember(Name="numVertices")]
        public Vec2Dto numVertices { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class ExportParameters {\n");
            sb.Append("  EmbossedText: ").Append(embossedText).Append("\n");
            sb.Append("  fileName: ").Append(fileName).Append("\n");
            sb.Append("  ExportFormat: ").Append(exportFormat).Append("\n");
            sb.Append("  FontSize: ").Append(fontSize).Append("\n");
            sb.Append("  HasPlantarPattern: ").Append(hasPlantarPattern).Append("\n");
            sb.Append("  NumVertices: ").Append(numVertices).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((ExportParametersDto)obj);
        }

        public bool Equals(ExportParametersDto other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    embossedText == other.embossedText ||
                    embossedText != null &&
                    embossedText.Equals(other.embossedText)
                ) 
                &&
                (
                    fileName == other.fileName ||
                    fileName != null &&
                    fileName.Equals(other.fileName)
                ) 
                && 
                (
                    fontSize == other.fontSize ||
                    fontSize != null &&
                    fontSize.Equals(other.fontSize)
                ) && 
                (
                    exportFormat == other.exportFormat ||
                    exportFormat != null &&
                    exportFormat.Equals(other.exportFormat)
                ) && 
                (
                    hasPlantarPattern == other.hasPlantarPattern ||
                    hasPlantarPattern != null &&
                    hasPlantarPattern.Equals(other.hasPlantarPattern)
                ) && 
                (
                    numVertices == other.numVertices ||
                    numVertices != null &&
                    numVertices.Equals(other.numVertices)
                );
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = 41;
                    if (embossedText != null)
                    hashCode = hashCode * 59 + embossedText.GetHashCode();
                    if (fileName != null) 
                    hashCode = hashCode * 59 + fileName.GetHashCode();
                    if (exportFormat != null)
                    hashCode = hashCode * 59 + exportFormat.GetHashCode(); 
                    if (fontSize != null)
                    hashCode = hashCode * 59 + fontSize.GetHashCode();
                    if (hasPlantarPattern != null)
                    hashCode = hashCode * 59 + hasPlantarPattern.GetHashCode();
                    if (numVertices != null)
                    hashCode = hashCode * 59 + numVertices.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(ExportParametersDto left, ExportParametersDto right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(ExportParametersDto left, ExportParametersDto right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
