using System;
using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;

namespace Leroy.Dto
{
    [DataContract]
    public class ExportDto :  IEquatable<ExportDto>
    {
        
        [DataMember(Name="fileId")]
        public string fileId { get; set; }

        [DataMember(Name="fileName")]
        public string fileName { get; set; }

        [DataMember(Name="downloadUrl")]
        public string downloadUrl { get; set; }
        
        [DataMember(Name="insole")]
        public string insole { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class ExportDto {\n");
            sb.Append("  fileId: ").Append(fileId).Append("\n");
            sb.Append("  fileName: ").Append(fileName).Append("\n");
            sb.Append("  downloadUrl: ").Append(downloadUrl).Append("\n");
            sb.Append("  insole: ").Append(insole).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        public override bool Equals(object input)
        {
            return this.Equals(input as ExportDto);
        }

        public bool Equals(ExportDto input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.insole == input.insole ||
                    (this.insole != null &&
                    this.insole.Equals(input.insole))
                )
                && 
                (
                    this.fileId == input.fileId ||
                    (this.fileId != null &&
                     this.fileId.Equals(input.fileId))
                )&& 
                (
                    this.fileName == input.fileName ||
                    (this.fileName != null &&
                    this.fileName.Equals(input.fileName))
                ) && 
                (
                    this.downloadUrl == input.downloadUrl ||
                    (this.downloadUrl != null &&
                    this.downloadUrl.Equals(input.downloadUrl))
                );
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = 41;
                if (this.insole != null)
                    hashCode = hashCode * 59 + this.insole.GetHashCode();
                if (this.fileId != null)
                    hashCode = hashCode * 59 + this.fileId.GetHashCode();
                if (this.fileName != null)
                    hashCode = hashCode * 59 + this.fileName.GetHashCode();
                if (this.downloadUrl != null)
                    hashCode = hashCode * 59 + this.downloadUrl.GetHashCode();
                return hashCode;
            }
        }

    }

}
