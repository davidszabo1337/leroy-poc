using System;
using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;

namespace Leroy.Dto
{ 
    [DataContract]
    public class ErrorResponseDto : IEquatable<ErrorResponseDto>
    { 
        [DataMember(Name="code")]
        public int code { get; set; }

        [DataMember(Name="message")]
        public string message { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Error {\n");
            sb.Append("  Code: ").Append(code).Append("\n");
            sb.Append("  Message: ").Append(message).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((ErrorResponseDto)obj);
        }

        public bool Equals(ErrorResponseDto other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return 
                (
                    code == other.code ||
                    code != null &&
                    code.Equals(other.code)
                ) && 
                (
                    message == other.message ||
                    message != null &&
                    message.Equals(other.message)
                );
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = 41;
                    if (code != null)
                    hashCode = hashCode * 59 + code.GetHashCode();
                    if (message != null)
                    hashCode = hashCode * 59 + message.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(ErrorResponseDto left, ErrorResponseDto right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(ErrorResponseDto left, ErrorResponseDto right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
