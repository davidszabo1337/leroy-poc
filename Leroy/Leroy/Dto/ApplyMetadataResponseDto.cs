using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using Newtonsoft.Json;

namespace Leroy.Dto
{ 
    [DataContract]
    public class ApplyMetadataResponseDto : IEquatable<ApplyMetadataResponseDto>
    {
        [DataMember(Name = "insoleData")]
        public List<string> insoleData { get; set; } = new List<string>();

        [DataMember(Name= "metaData")]
        public MetadataDto metaData { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class ApplyMetadataResponse {\n");
            sb.Append("  InsoleData: ").Append(insoleData).Append("\n");
            sb.Append("  LeftFootMetadata: ").Append(metaData).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((ApplyMetadataResponseDto)obj);
        }
        public bool Equals(ApplyMetadataResponseDto other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;

            return
                (
                    insoleData == other.insoleData ||
                    insoleData != null &&
                    insoleData.Equals(other.insoleData)
                ) &&
                (
                    metaData == other.metaData ||
                    metaData != null &&
                    metaData.Equals(other.metaData)
                ); 
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = 41;
                    if (insoleData != null)
                    hashCode = hashCode * 59 + insoleData.GetHashCode();
                    if (metaData != null)
                    hashCode = hashCode * 59 + metaData.GetHashCode();
                return hashCode;
            }
        }

        #region Operators
        #pragma warning disable 1591

        public static bool operator ==(ApplyMetadataResponseDto left, ApplyMetadataResponseDto right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(ApplyMetadataResponseDto left, ApplyMetadataResponseDto right)
        {
            return !Equals(left, right);
        }

        #pragma warning restore 1591
        #endregion Operators
    }
}
