﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Leroy.Dto;
using Leroy.Service;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;

namespace Leroy.Feature
{
    public class DesignServiceFeature : IDesignServiceFeature
    {
        
        private IWebHostEnvironment hostEnvironment;

        public DesignServiceFeature(IWebHostEnvironment hostEnvironment)
        {
            this.hostEnvironment = hostEnvironment;
        }

        public async Task<MetadataDto> calcLandmarkPoints(MetadataDto dto)
        {
            throw new System.NotImplementedException();
        }

        public async Task<MetadataDto> selectTemplateBasedOnLandmarks(MetadataDto dto)
        {
            throw new System.NotImplementedException();
        }

        public async Task<MetadataDto> getInsole(MetadataDto dto)
        {
            throw new System.NotImplementedException();
        }

        public async Task<ApplyMetadataResponseDto> generateInsole(MetadataDto dto)
        {
            ApplyMetadataResponseDto responseDto = new ApplyMetadataResponseDto();

            string path = Path.Combine(hostEnvironment.ContentRootPath, "Feature\\insole.txt");
            var insoleData = File.ReadAllLines(path);
            responseDto.insoleData = insoleData.ToList();
            return responseDto;
        }

        public async Task<ExportDto> exportInsole(MetadataDto dto)
        {
            throw new System.NotImplementedException();
        }
    }
}