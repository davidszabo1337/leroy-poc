using System;
using Leroy.Dto;

namespace Leroy.Exceptions
{
    class ErrorBuilder
    {
        // private static ILogger logger => LogManager.GetLogger(nameof(ErrorBuilder));
        
        public static ErrorResponseDto buildFromException(Exception e)
        {
            ErrorResponseDto error = new ErrorResponseDto();
            
            // logger.Error("Error occured" + Environment.NewLine 
            //                              + "Type was: " + e.GetType().FullName + Environment.NewLine 
            //                              + "Message was: " + e.Message + Environment.NewLine
            //                              + "Source was: " +  e.Source + Environment.NewLine
            //                              + "Inner exception: " +  e.InnerException + Environment.NewLine
            //                              + "Stacktrace: " + e.StackTrace + Environment.NewLine);
            string type = e.GetType().Name;
            switch (type)
            {
                //TODO: should change the error msg
                case "JsonSerializationException":
                case "JsonReaderException":
                case "ArgumentException":
                case "ValidationException":
                case "FormatException":
                    error.code = 400;
                    error.message = e.Message;
                    break;
                case "AuthException":
                    error.code = 401;
                    error.message = e.Message;
                    break;
                default:
                    error.code = 500;
                    error.message = e.Message;
                    break;
            }
            return error;
        }
    }
}
