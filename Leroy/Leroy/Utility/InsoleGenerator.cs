// using System;
// using System.Collections.Generic;
// using Leroy.Model;
//
// namespace Leroy.Utility
// {
//     public class InsoleGenerator : MonoBehaviour
//     {
//         public Insole insole;
//         public InsoleSelector insoleSelector;
//
//         public ArchModification archTool;
//         public BevelShellEdge bevelShellEdgeTool;
//         public ForefootPosting forefootPostingTool;
//         public FirstRayCutout firstRayCutoutTool;
//         public HeelCup heelSeatTool;
//         public HeelPosting heelPostingTool;
//         public Length lengthWidthTool;
//         public MedialFlange medialFlangeTool;
//         public LateralFlange lateralFlangeTool;
//         public RegionBasedWidth regionBasedWidthTool;
//         public ShellThickness shellThicknessTool;
//         public HeelSkive heelSkiveTool;
//         
//         public string ExportInsole(Metadata metadata)
//         {
//             // select left foot and apply parameters
//             string insoleName = metadata.insoleName;
//             if (insoleName == "")
//             {
//                 insoleName = (metadata.side == FootScan.FootSide.Left) ? "L-148-47-3-14" : "R-148-47-3-14";
//             }
//             insoleSelector.SelectInsoleByName(insoleName);
//
//             archTool.SetValue_ArchHeight(metadata.insoleParameters.adjustArchHeight, false);
//             archTool.SetValue_ApexPosition(metadata.insoleParameters.adjustArchApexPosition, false);
//
//             bevelShellEdgeTool.SetValue_Strength(metadata.insoleParameters.bevelShellEdgeThickness, false);
//             bevelShellEdgeTool.SetValue_Distance(metadata.insoleParameters.bevelShellEdgeDistance, false);
//
//             forefootPostingTool.SetValue(metadata.insoleParameters.forefootPostingAngle, false);
//
//             firstRayCutoutTool.SetValue(metadata.insoleParameters.firstRayCutout, false);
//
//             heelSeatTool.SetValue_Height(metadata.insoleParameters.heelSeatHeight);
//
//             heelSkiveTool.SetValue_Enabled(metadata.insoleParameters.heelSkiveEnabled, false);
//             heelSkiveTool.SetValue_IsMedial(metadata.insoleParameters.heelSkiveIsMedial, false);
//             heelSkiveTool.SetValue_Depth(metadata.insoleParameters.heelSkiveDepth, false);
//             heelSkiveTool.SetValue_Angle(metadata.insoleParameters.heelSkiveAngle, false);
//             heelSkiveTool.SetValue_Tilt(metadata.insoleParameters.heelSkiveTilt, false);
//
//             heelPostingTool.SetAlpha(metadata.insoleParameters.heelPostingAlpha);
//             heelPostingTool.SetWidth(metadata.insoleParameters.heelPostingWidth);
//             heelPostingTool.SetGamma(metadata.insoleParameters.heelPostingGamma);
//             heelPostingTool.SetDelta(metadata.insoleParameters.heelPostingDelta);
//             heelPostingTool.SetEnabled(metadata.insoleParameters.heelPostingEnabled);
//
//             lengthWidthTool.SetValue_Length(metadata.insoleParameters.lengthWidthLength, false);
//             lengthWidthTool.SetValue_Width(metadata.insoleParameters.lengthWidthWidth, false);
//
//             medialFlangeTool.SetEnabled(metadata.insoleParameters.medialFlangeEnabled, true);
//             medialFlangeTool.SetValue_Height(metadata.insoleParameters.medialFlangeHeight, false);
//             medialFlangeTool.SetValue_Width(metadata.insoleParameters.medialFlangeWidth, false);
//             medialFlangeTool.SetValue_Thickness(metadata.insoleParameters.medialFlangeThickness, false);
//
//             lateralFlangeTool.SetEnabled(metadata.insoleParameters.lateralFlangeEnabled, true);
//             lateralFlangeTool.SetValue_Height(metadata.insoleParameters.lateralFlangeHeight, false);
//             lateralFlangeTool.SetValue_Width(metadata.insoleParameters.lateralFlangeWidth, false);
//             lateralFlangeTool.SetValue_Thickness(metadata.insoleParameters.lateralFlangeThickness, false);
//
//             regionBasedWidthTool.SetValue_ProximalPoint(metadata.insoleParameters.regionBasedWidthProximalPoint, false);
//             regionBasedWidthTool.SetValue_DistalPoint(metadata.insoleParameters.regionBasedWidthDistalPoint, false);
//             regionBasedWidthTool.SetValue_ExtraWidth(metadata.insoleParameters.regionBasedWidthWidth, false);
//             regionBasedWidthTool.SetValue_Smoothing(metadata.insoleParameters.regionBasedWidthSmoothing, false);
//
//             shellThicknessTool.SetValue(metadata.insoleParameters.shellThickness, false);
//
//             string dataStr;
//             switch (metadata.exportParameters.exportFormat)
//             {
//                 case ExportParameters.ExportFormat.OBJ:
//                     dataStr = insole.ExportOBJ(metadata.exportParameters.embossedText, metadata.exportParameters.hasPlantarPattern, metadata.exportParameters.numVertices, metadata.exportParameters.fontSize);
//                     break;
//                 case ExportParameters.ExportFormat.STL:
//                     byte[] data = insole.ExportSTL(metadata.exportParameters.embossedText, metadata.exportParameters.hasPlantarPattern, metadata.exportParameters.numVertices, metadata.exportParameters.fontSize);
//                     dataStr = System.Convert.ToBase64String(data);
//                     break;
//                 default:
//                     throw new ArgumentOutOfRangeException();
//             }
//
//             return dataStr;
//         }
//
//         
//         public InsoleData GenerateInsole(Metadata metadata)
//         {
//             InsoleData insoleData = new InsoleData();
//
//             SetupTools(metadata);
//             
//             insoleData.metaData = metadata;
//             
//             insoleData.insoleData = GetInsoleSurfaces(insole);
//             return insoleData;
//         }
//
//         private void SetupTools(Metadata metadata)
//         {
//             string insoleName = metadata.insoleName;
//             if (insoleName == "")
//             {
//                 insoleName = (metadata.side == FootScan.FootSide.Left) ? "L-148-47-3-14" : "R-148-47-3-14";
//             }
//             insoleSelector.SelectInsoleByName(insoleName);
//
//             archTool.SetValue_ArchHeight(metadata.insoleParameters.adjustArchHeight, false);
//             archTool.SetValue_ApexPosition(metadata.insoleParameters.adjustArchApexPosition, false);
//
//             bevelShellEdgeTool.SetValue_Strength(metadata.insoleParameters.bevelShellEdgeThickness, false);
//             bevelShellEdgeTool.SetValue_Distance(metadata.insoleParameters.bevelShellEdgeDistance, false);
//
//             forefootPostingTool.SetValue(metadata.insoleParameters.forefootPostingAngle, false);
//
//             firstRayCutoutTool.SetValue(metadata.insoleParameters.firstRayCutout, false);
//
//             heelSeatTool.SetValue_Height(metadata.insoleParameters.heelSeatHeight, false);
//
//             heelSkiveTool.SetValue_Enabled(metadata.insoleParameters.heelSkiveEnabled, false);
//             heelSkiveTool.SetValue_IsMedial(metadata.insoleParameters.heelSkiveIsMedial, false);
//             heelSkiveTool.SetValue_Depth(metadata.insoleParameters.heelSkiveDepth, false);
//             heelSkiveTool.SetValue_Angle(metadata.insoleParameters.heelSkiveAngle, false);
//             heelSkiveTool.SetValue_Tilt(metadata.insoleParameters.heelSkiveTilt, false);
//
//             heelPostingTool.SetAlpha(metadata.insoleParameters.heelPostingAlpha);
//             heelPostingTool.SetWidth(metadata.insoleParameters.heelPostingWidth);
//             heelPostingTool.SetGamma(metadata.insoleParameters.heelPostingGamma);
//             heelPostingTool.SetDelta(metadata.insoleParameters.heelPostingDelta);
//             if (metadata.insoleParameters.heelPostingDelta == 0.0f)
//             {
//                 heelPostingTool.setDelta = true;
//             }
//             heelPostingTool.SetEnabled(metadata.insoleParameters.heelPostingEnabled);
//
//             lengthWidthTool.SetValue_Length(metadata.insoleParameters.lengthWidthLength, false);
//             lengthWidthTool.SetValue_Width(metadata.insoleParameters.lengthWidthWidth, false);
//
//             medialFlangeTool.SetEnabled(metadata.insoleParameters.medialFlangeEnabled, true, false);
//             medialFlangeTool.SetValue_Height(metadata.insoleParameters.medialFlangeHeight, false);
//             medialFlangeTool.SetValue_Width(metadata.insoleParameters.medialFlangeWidth, false);
//             medialFlangeTool.SetValue_Thickness(metadata.insoleParameters.medialFlangeThickness, false);
//
//             lateralFlangeTool.SetEnabled(metadata.insoleParameters.lateralFlangeEnabled, true, false);
//             lateralFlangeTool.SetValue_Height(metadata.insoleParameters.lateralFlangeHeight, false);
//             lateralFlangeTool.SetValue_Width(metadata.insoleParameters.lateralFlangeWidth, false);
//             lateralFlangeTool.SetValue_Thickness(metadata.insoleParameters.lateralFlangeThickness, false);
//
//             regionBasedWidthTool.SetValue_ProximalPoint(metadata.insoleParameters.regionBasedWidthProximalPoint, false);
//             regionBasedWidthTool.SetValue_DistalPoint(metadata.insoleParameters.regionBasedWidthDistalPoint, false);
//             regionBasedWidthTool.SetValue_ExtraWidth(metadata.insoleParameters.regionBasedWidthWidth, false);
//             regionBasedWidthTool.SetValue_Smoothing(metadata.insoleParameters.regionBasedWidthSmoothing, false);
//
//             shellThicknessTool.SetValue(metadata.insoleParameters.shellThickness, false);
//
//             insole.RecalculateMesh(insole.numVertices);
//         }
//
//         private List<string> GetInsoleSurfaces(Insole insole)
//         {
//             List<string> surfaces = new List<string>();
//             Mesh topSurfaceMesh = insole.topSurfaceObject.GetComponent<MeshFilter>().sharedMesh;
//             Mesh bottomSurfaceMesh = insole.bottomSurfaceObject.GetComponent<MeshFilter>().sharedMesh;
//             Mesh sideSurfaceMesh = insole.sideSurfaceObject.GetComponent<MeshFilter>().sharedMesh;
//
//             surfaces.Add(System.Convert.ToBase64String(
//                 Converter.Compress(
//                     Converter.MeshGeometryDataToByteArray(topSurfaceMesh, Converter.MeshGeometryDataToByteArrayFlags.HasNormals))));
//
//             surfaces.Add(System.Convert.ToBase64String(
//                  Converter.Compress(
//                      Converter.MeshGeometryDataToByteArray(bottomSurfaceMesh, Converter.MeshGeometryDataToByteArrayFlags.HasNormals))));
//
//             surfaces.Add(System.Convert.ToBase64String(
//                 Converter.Compress(
//                     Converter.MeshGeometryDataToByteArray(sideSurfaceMesh, Converter.MeshGeometryDataToByteArrayFlags.HasNormals))));
//        
//             if (medialFlangeTool.isEnabled)
//             {
//                 Mesh medialFlangeTopSurfaceMesh = insole.medialFlangeTopSurfaceGameObject.GetComponent<MeshFilter>().sharedMesh;
//                 Mesh medialFlangeBottomSurfaceMesh = insole.medialFlangeBottomSurfaceGameObject.GetComponent<MeshFilter>().sharedMesh;
//                 Mesh medialFlangeSideSurfaceMesh = insole.medialFlangeSideSurfaceGameObject.GetComponent<MeshFilter>().sharedMesh;
//                 
//                 surfaces.Add(System.Convert.ToBase64String(
//                      Converter.Compress(
//                          Converter.MeshGeometryDataToByteArray(medialFlangeTopSurfaceMesh, Converter.MeshGeometryDataToByteArrayFlags.HasNormals))));
//
//                 surfaces.Add(System.Convert.ToBase64String(
//                       Converter.Compress(
//                           Converter.MeshGeometryDataToByteArray(medialFlangeBottomSurfaceMesh, Converter.MeshGeometryDataToByteArrayFlags.HasNormals))));
//
//                 surfaces.Add(System.Convert.ToBase64String(
//                     Converter.Compress(
//                         Converter.MeshGeometryDataToByteArray(medialFlangeSideSurfaceMesh, Converter.MeshGeometryDataToByteArrayFlags.HasNormals))));
//             }
//
//             if (lateralFlangeTool.isEnabled)
//             {
//                 Mesh lateralFlangeTopSurfaceMesh = insole.lateralFlangeTopSurfaceGameObject.GetComponent<MeshFilter>().sharedMesh;
//                 Mesh lateralFlangeBottomSurfaceMesh = insole.lateralFlangeBottomSurfaceGameObject.GetComponent<MeshFilter>().sharedMesh;
//                 Mesh lateralFlangeSideSurfaceMesh = insole.lateralFlangeSideSurfaceGameObject.GetComponent<MeshFilter>().sharedMesh;
//
//                 surfaces.Add(System.Convert.ToBase64String(
//                      Converter.Compress(
//                          Converter.MeshGeometryDataToByteArray(lateralFlangeTopSurfaceMesh, Converter.MeshGeometryDataToByteArrayFlags.HasNormals))));
//
//                 surfaces.Add(System.Convert.ToBase64String(
//                      Converter.Compress(
//                          Converter.MeshGeometryDataToByteArray(lateralFlangeBottomSurfaceMesh, Converter.MeshGeometryDataToByteArrayFlags.HasNormals))));
//
//                 surfaces.Add(System.Convert.ToBase64String(
//                      Converter.Compress(
//                          Converter.MeshGeometryDataToByteArray(lateralFlangeSideSurfaceMesh, Converter.MeshGeometryDataToByteArrayFlags.HasNormals))));
//               
//             }
//           
//             return surfaces;
//         }
//     }
// }
