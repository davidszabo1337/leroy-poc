using Leroy.Dto;

namespace Leroy.Utility
{
    partial class Updater
    {
        public static void updateInsoleParamsToDefaults(MetadataDto metadata)
        {
            metadata.insoleParameters = getDefaultInsoleParameter();
        }
        private static InsoleParametersDto getDefaultInsoleParameter() 
        {
            InsoleParametersDto dto = new InsoleParametersDto();
            dto.adjustArchHeight = 0.0f;
            dto.adjustArchApexPosition = 0.0f;
            dto.forefootPostingAngle = 0f;
            dto.shellThickness = 0.2f;
            dto.firstRayCutout = false;
            dto.bevelShellEdgeThickness = 0.0f;
            dto.bevelShellEdgeDistance = 1.15f;
            dto.lengthWidthLength = 1.0f;
            dto.lengthWidthWidth = 1.0f;
            dto.heelPostingAlpha = 0.0f;
            dto.heelPostingWidth = 0.0f;
            dto.heelPostingGamma = 15.0f;
            dto.heelPostingDelta = 0.0f;
            dto.heelPostingEnabled = false;
            dto.regionBasedWidthProximalPoint = -2.0f;
            dto.regionBasedWidthDistalPoint = 2.0f;
            dto.regionBasedWidthWidth = 0.0f;
            dto.regionBasedWidthSmoothing = 1.0f;
            dto.medialFlangeEnabled = false;
            dto.medialFlangeHeight = 1.27f;
            dto.medialFlangeWidth = 0.635f;
            dto.medialFlangeThickness = 0.2f;
            dto.lateralFlangeEnabled = false;
            dto.lateralFlangeHeight = 0.635f;
            dto.lateralFlangeWidth = 0.3175f;
            dto.lateralFlangeThickness = 0.2f;
            dto.heelSeatHeight = 0.2f;
            dto.heelSkiveEnabled = false;
            dto.heelSkiveIsMedial = true;
            dto.heelSkiveDepth = 0.0f;
            dto.heelSkiveAngle = 15.0f;
            dto.heelSkiveTilt = 0.0f;
            return dto;
        }
    }
}
