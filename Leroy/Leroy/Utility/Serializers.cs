using Leroy.Dto;

namespace Leroy.Utility
{
    class Serializers
    {
        public static MetadataDto deserializeMetadataDto(string json)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<MetadataDto>(json,
                new Newtonsoft.Json.JsonSerializerSettings {MissingMemberHandling = Newtonsoft.Json.MissingMemberHandling.Error});
        }
        public static ApplyMetadataResponseDto deserializeApplyMetadataResponseDto(string json)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<ApplyMetadataResponseDto>(json,
                new Newtonsoft.Json.JsonSerializerSettings {MissingMemberHandling = Newtonsoft.Json.MissingMemberHandling.Error});
        }
        public static ExportParametersDto deserializeExportParametersDto(string json)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<ExportParametersDto>(json);
        }
        public static ExportDto deserializeExportDto(string json)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<ExportDto>(json);
        }
        public static FootScanParametersDto deserializeFootScanParametersDto(string json)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<FootScanParametersDto>(json);
        }
        public static InsoleParametersDto deserializeInsoleParametersDto(string json)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<InsoleParametersDto>(json);
        }
    }
}
