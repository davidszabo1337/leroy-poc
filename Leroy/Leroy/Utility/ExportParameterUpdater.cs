using Leroy.Dto;

namespace Leroy.Utility
{
    partial class Updater
    {
        public static void updateExportParamsToDefaults(MetadataDto metadata)
        {
            metadata.exportParameters = getDefaultExportParams(metadata);
        }
        static private ExportParametersDto getDefaultExportParams(MetadataDto metadata) 
        {
            ExportParametersDto dto = new ExportParametersDto();
            dto.fileName = "insole_" + metadata.side.ToLower() + "_" + metadata.measurementId;
            dto.embossedText = "";
            dto.exportFormat = "stl";
            dto.fontSize = 1.2f;
            dto.hasPlantarPattern = false;
            Vec2Dto vec2 = new Vec2Dto(128, 64);
            dto.numVertices = vec2;
            return dto;
        }
    }
}
