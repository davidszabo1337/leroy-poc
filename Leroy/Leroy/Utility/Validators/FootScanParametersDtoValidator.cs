// unset

using System.Collections.Generic;
using FluentValidation;
using Leroy.Dto;
using Leroy.Model;

namespace Leroy.Utility.Validators
{
    public class FootScanParametersDtoValidator : AbstractValidator<FootScanParametersDto>
    {
        private readonly List<string> axes = new List<string>() {"X", "Y", "Z", "NegX", "NegY", "NegZ"};
        
        public FootScanParametersDtoValidator()
        {
            RuleFor(dto => dto.upDirection)
                .NotNull()
                .WithMessage("missing field: upDirection");
            
            RuleFor(dto => dto.forwardDirection)
                .NotNull()
                .WithMessage("missing field: forwardDirection");

            RuleFor(dto => dto.rightDirection)
                .NotNull()
                .WithMessage("missing field: rightDirection");

            RuleFor(dto => dto).Must(dto => areAxisValid(dto)).WithMessage("Axes are invalid");
        }

        private bool areAxisValid(FootScanParametersDto dto)
        {
            bool areAxeNamesAreValid = (axes.Contains(dto.forwardDirection) && axes.Contains(dto.rightDirection) &&
                                        axes.Contains(dto.upDirection));

            if (!areAxeNamesAreValid)
            {
                return false;
            }
                
            ImportAxisDirection upAxis = Mapper.diretionMap[dto.upDirection];
            ImportAxisDirection forwardAxis = Mapper.diretionMap[dto.forwardDirection];
            ImportAxisDirection rightAxis = Mapper.diretionMap[dto.rightDirection];


            int upAxisDirection = (int)upAxis % 3;
            int forwardAxisDirection = (int)forwardAxis % 3;
            int rightAxisDirection = (int)rightAxis % 3;
            return !(upAxisDirection == forwardAxisDirection || forwardAxisDirection == rightAxisDirection ||
                    upAxisDirection == rightAxisDirection);
        }
    }
}