// unset

using FluentValidation;
using Leroy.Dto;

namespace Leroy.Utility.Validators
{
    public class InsoleParametersDtoValidator : AbstractValidator<InsoleParametersDto>
    {
        public InsoleParametersDtoValidator()
        {
            RuleFor(dto => dto.adjustArchHeight)
                .NotNull()
                .WithMessage("missing field: adjustArchHeight");
            
            RuleFor(dto => dto.adjustArchApexPosition)
                .NotNull()
                .WithMessage("missing field: adjustArchApexPosition");
            
            RuleFor(dto => dto.forefootPostingAngle)
                .NotNull()
                .WithMessage("missing field: forefootPostingAngle");
            
            RuleFor(dto => dto.shellThickness)
                .NotNull()
                .WithMessage("missing field: shellThickness");
            
            RuleFor(dto => dto.firstRayCutout)
                .NotNull()
                .WithMessage("missing field: firstRayCutout");
            
            RuleFor(dto => dto.bevelShellEdgeThickness)
                .NotNull()
                .WithMessage("missing field: bevelShellEdgeThickness");
            
            RuleFor(dto => dto.bevelShellEdgeDistance)
                .NotNull()
                .WithMessage("missing field: bevelShellEdgeDistance");
             
            RuleFor(dto => dto.lengthWidthLength)
                .NotNull()
                .WithMessage("missing field: lengthWidthLength");
             
            RuleFor(dto => dto.lengthWidthWidth)
                .NotNull()
                .WithMessage("missing field: lengthWidthWidth");
             
            RuleFor(dto => dto.heelPostingAlpha)
                .NotNull()
                .WithMessage("missing field: heelPostingAlpha");
             
            RuleFor(dto => dto.heelPostingWidth)
                .NotNull()
                .WithMessage("missing field: heelPostingWidth");
             
            RuleFor(dto => dto.heelPostingGamma)
                .NotNull()
                .WithMessage("missing field: heelPostingGamma");
             
            RuleFor(dto => dto.heelPostingDelta)
                .NotNull()
                .WithMessage("missing field: heelPostingDelta");
             
            RuleFor(dto => dto.heelPostingEnabled)
                .NotNull()
                .WithMessage("missing field: heelPostingEnabled");
             
            RuleFor(dto => dto.regionBasedWidthProximalPoint)
                .NotNull()
                .WithMessage("missing field: regionBasedWidthProximalPoint");
             
            RuleFor(dto => dto.regionBasedWidthDistalPoint)
                .NotNull()
                .WithMessage("missing field: regionBasedWidthDistalPoint");
             
            RuleFor(dto => dto.regionBasedWidthWidth)
                .NotNull()
                .WithMessage("missing field: regionBasedWidthWidth");
             
            RuleFor(dto => dto.regionBasedWidthSmoothing)
                .NotNull()
                .WithMessage("missing field: regionBasedWidthSmoothing");
              
            RuleFor(dto => dto.medialFlangeEnabled)
                .NotNull()
                .WithMessage("missing field: medialFlangeEnabled");
              
            RuleFor(dto => dto.medialFlangeHeight)
                .NotNull()
                .WithMessage("missing field: medialFlangeHeight");
              
            RuleFor(dto => dto.medialFlangeWidth)
                .NotNull()
                .WithMessage("missing field: medialFlangeWidth");
              
            RuleFor(dto => dto.medialFlangeThickness)
                .NotNull()
                .WithMessage("missing field: medialFlangeThickness");
              
            RuleFor(dto => dto.lateralFlangeEnabled)
                .NotNull()
                .WithMessage("missing field: lateralFlangeEnabled");
              
            RuleFor(dto => dto.lateralFlangeHeight)
                .NotNull()
                .WithMessage("missing field: lateralFlangeHeight");
              
            RuleFor(dto => dto.lateralFlangeWidth)
                .NotNull()
                .WithMessage("missing field: lateralFlangeWidth");
              
            RuleFor(dto => dto.lateralFlangeThickness)
                .NotNull()
                .WithMessage("missing field: lateralFlangeThickness");
              
            RuleFor(dto => dto.heelSeatHeight)
                .NotNull()
                .WithMessage("missing field: heelSeatHeight");
               
            RuleFor(dto => dto.heelSkiveEnabled)
                .NotNull()
                .WithMessage("missing field: heelSkiveEnabled");
               
            RuleFor(dto => dto.heelSkiveIsMedial)
                .NotNull()
                .WithMessage("missing field: heelSkiveIsMedial");
               
            RuleFor(dto => dto.heelSkiveDepth)
                .NotNull()
                .WithMessage("missing field: heelSkiveDepth");
               
            RuleFor(dto => dto.heelSkiveAngle)
                .NotNull()
                .WithMessage("missing field: heelSkiveAngle");
               
            RuleFor(dto => dto.heelSkiveTilt)
                .NotNull()
                .WithMessage("missing field: heelSkiveTilt");
                
        }
    }
}