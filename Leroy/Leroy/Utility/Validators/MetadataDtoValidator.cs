// unset

using System;
using System.Collections.Generic;
using FluentValidation;
using Leroy.Dto;

namespace Leroy.Utility.Validators
{
    public class MetadataDtoValidator : AbstractValidator<MetadataDto>
    {
        private readonly List<string> footSides = new List<string>() {"Left", "Right"};
        
        public MetadataDtoValidator()
        {
            RuleFor(metadata => metadata.measurementId)
                .NotNull()
                .NotEmpty()
                .WithMessage("missing field: measurementId");
            
            RuleFor(metadata => metadata.side)
                .Must(side => footSides.Contains(side))
                .WithMessage("Side must be: " + String.Join(", ", footSides));
            
            RuleFor(metadata => metadata.footScanParameters)
                .NotNull()
                .WithMessage("missing field: footScanParameters");

            RuleFor(metadata => metadata.footScanParameters)
                .SetValidator(new FootScanParametersDtoValidator());
        }
    }
}