// unset

using System;
using System.Collections.Generic;
using FluentValidation;
using Leroy.Dto;

namespace Leroy.Utility.Validators
{
    public class ExportParametersDtoValidator : AbstractValidator<ExportParametersDto>
    {
        private readonly List<string> exportFormats = new List<string>() {"obj", "stl"};
        
        public ExportParametersDtoValidator()
        {
            RuleFor(dto => dto.embossedText)
                .NotNull()
                .WithMessage("missing field: embossedText");
            
            RuleFor(dto => dto.exportFormat)
                .NotNull()
                .WithMessage("missing field: exportFormat");
            RuleFor(dto => dto.exportFormat)
                .Must(format => exportFormats.Contains(format))
                .WithMessage("export format must be one of: " + String.Join(", ", exportFormats));
            
            RuleFor(dto => dto.fontSize)
                .NotNull()
                .WithMessage("missing field: fontSize");
            
            RuleFor(dto => dto.hasPlantarPattern)
                .NotNull()
                .WithMessage("missing field: hasPlantarPattern");
            
            RuleFor(dto => dto.numVertices)
                .NotNull()
                .WithMessage("missing field: numVertices");
        }
    }
}