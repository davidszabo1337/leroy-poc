// unset

using FluentValidation;
using Leroy.Dto;

namespace Leroy.Utility.Validators
{
    public class Validator : IValidator
    {
        private MetadataDtoValidator metadataDtoValidator;
        private FootScanParametersDtoValidator footScanParametersDtoValidator;
        private InsoleParametersDtoValidator insoleParametersDtoValidator;
        private ExportParametersDtoValidator exportParametersDtoValidator;

        public Validator()
        {
            metadataDtoValidator = new MetadataDtoValidator();
            footScanParametersDtoValidator = new FootScanParametersDtoValidator();
            insoleParametersDtoValidator = new InsoleParametersDtoValidator();
            exportParametersDtoValidator = new ExportParametersDtoValidator();
        }

        public void validateMetadataDto(MetadataDto dto)
        {
            metadataDtoValidator.ValidateAndThrow(dto);
        }
        
        public void validateFootScanParametersDto(FootScanParametersDto dto)
        {
            footScanParametersDtoValidator.ValidateAndThrow(dto);
        }
        
        public void validateInsoleParametersDto(InsoleParametersDto dto)
        {
            insoleParametersDtoValidator.ValidateAndThrow(dto);
        }
        
        public void validateExportParametersDto(ExportParametersDto dto)
        {
            exportParametersDtoValidator.ValidateAndThrow(dto);
        }

        public void fullyValidateMetadata(MetadataDto dto)
        {
            metadataDtoValidator.ValidateAndThrow(dto);
            if (dto.footScanParameters == null)
            {
                throw new ValidationException("missing field: footScanParameters");
            }
            footScanParametersDtoValidator.ValidateAndThrow(dto.footScanParameters);
            if (dto.exportParameters == null)
            {
                throw new ValidationException("missing field: exportParameters");
            }
            exportParametersDtoValidator.ValidateAndThrow(dto.exportParameters);
            if (dto.insoleParameters == null)
            {
                throw new ValidationException("missing field: insoleParameters");
            }
            insoleParametersDtoValidator.ValidateAndThrow(dto.insoleParameters);
        }
    }
}