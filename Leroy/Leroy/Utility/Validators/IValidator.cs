// unset

using Leroy.Dto;

namespace Leroy.Utility.Validators
{
    public interface IValidator
    {
        void validateMetadataDto(MetadataDto dto);
        void validateFootScanParametersDto(FootScanParametersDto dto);
        void validateInsoleParametersDto(InsoleParametersDto dto);
        void validateExportParametersDto(ExportParametersDto dto);

        void fullyValidateMetadata(MetadataDto dto);
    }
}