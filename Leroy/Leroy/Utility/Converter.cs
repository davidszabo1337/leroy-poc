// // unset
//
// using System.IO;
// using System.IO.Compression;
// using System.Net;
//
// namespace Leroy.Utility
// {
//     public class Converter
//     {
//         private enum CompressionType
//         {
//             None, Gzip, Deflate /*, Brotli*/
//         }
//
//         private static CompressionType SelectCompressionType(HttpListenerContext context)
//         {
//             string encodings = context.Request.Headers["Accept-Encoding"];
//
//             if (encodings == null)
//             {
//                 return CompressionType.None;
//             }
//
//             bool hasGzip = false;
//             bool hasDeflate = false;
//             foreach (string encoding in encodings.Split(','))
//             {
//                 switch (encoding.Trim())
//                 {
//                     case "gzip":
//                         hasGzip = true;
//                         break;
//                     case "deflate":
//                         hasDeflate = true;
//                         break;
//                     default:
//                         break;
//                 }
//             }
//
//             return
//                 hasGzip
//                     ? CompressionType.Gzip
//                     : hasDeflate
//                         ? CompressionType.Deflate
//                         : CompressionType.None;
//         }
//
//         public static byte[] MeshGeometryDataToByteArray(Mesh mesh, MeshGeometryDataToByteArrayFlags flags)
//         {
//             Vector3[] vertices = mesh.vertices;
//             int[] indices = mesh.triangles;
//             Vector3[] normals = (flags & MeshGeometryDataToByteArrayFlags.HasNormals) != 0 ? mesh.normals : null;
//
//             return MeshGeometryDataToByteArray(vertices, indices, normals, flags);
//         }
//
//         public static byte[] MeshGeometryDataToByteArray(Vector3[] vertices, int[] indices, Vector3[] normals, MeshGeometryDataToByteArrayFlags flags)
//         {
//             // first 4 bytes - number of vertices
//             // second 4 bytes - number of indices
//             // next 1 byte - flags
//             // vertices data
//             // indices data
//             // normals data
//
//             bool hasNormals = (flags & MeshGeometryDataToByteArrayFlags.HasNormals) != 0;
//
//             int numVertices = vertices.Length;
//             int numIndices = indices.Length;
//             int verticesSizeInBytes = 4 * 3 * numVertices; // 4 * 3 bytes per vertex
//             int indicesSizeInBytes = 4 * numIndices; // 4 bytes per index
//
//             int numBytes =
//                 4 + // 1 int = 4 bytes for numVertices
//                 4 + // 1 int = 4 bytes for numIndices
//                 1 + // 1 byte for flags
//                 verticesSizeInBytes + // vertices
//                 indicesSizeInBytes + // indices
//                 (hasNormals ? verticesSizeInBytes : 0); // normals
//
//             byte[] bytes = new byte[numBytes];
//
//             unsafe
//             {
//                 fixed (byte* resultBytes = bytes)
//                 {
//                     UnsafeUtility.MemCpy(resultBytes, &numVertices, 4);
//                     UnsafeUtility.MemCpy(resultBytes + 4, &numIndices, 4);
//                     resultBytes[8] = (byte)flags;
//
//                     fixed (Vector3* ptr = vertices)
//                     {
//                         byte* bytePtr = (byte*)ptr;
//                         UnsafeUtility.MemCpy(resultBytes + 9, bytePtr, verticesSizeInBytes);
//                     }
//
//                     fixed (int* ptr = indices)
//                     {
//                         byte* bytePtr = (byte*)ptr;
//                         UnsafeUtility.MemCpy(resultBytes + 9 + verticesSizeInBytes, bytePtr, indicesSizeInBytes);
//                     }
//
//                     if (hasNormals)
//                     {
//                         fixed (Vector3* ptr = normals)
//                         {
//                             byte* bytePtr = (byte*)ptr;
//                             UnsafeUtility.MemCpy(resultBytes + 9 + verticesSizeInBytes + indicesSizeInBytes, bytePtr, verticesSizeInBytes);
//                         }
//                     }
//                 }
//             }
//
//             return bytes;
//         }
//
//         public enum MeshGeometryDataToByteArrayFlags : byte
//         {
//             None = 0,
//             HasNormals = 1
//         }
//
//         public static byte[] Compress(byte[] data)
//         {
//             using (MemoryStream compressedStream = new MemoryStream())
//             using (GZipStream gzipStream = new GZipStream(compressedStream, CompressionMode.Compress))
//             {
//                 gzipStream.Write(data, 0, data.Length);
//                 gzipStream.Close();
//                 return compressedStream.ToArray();
//             }
//         }
//
//         public static byte[] Decompress(byte[] data)
//         {
//             using (MemoryStream compressedStream = new MemoryStream(data))
//             using (GZipStream gzipStream = new GZipStream(compressedStream, CompressionMode.Decompress))
//             using (MemoryStream resultStream = new MemoryStream())
//             {
//                 gzipStream.CopyTo(resultStream);
//                 return resultStream.ToArray();
//             }
//         }
//     }
// }