using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using Leroy.Dto;
using Leroy.Model;

namespace Leroy.Utility
{
    public class Mapper
    {
        public static FootScanParameters mapToFootScanParameters(FootScanParametersDto dto)
        {
            FootScanParameters param = new FootScanParameters();
            param.upDirection = diretionMap[dto.upDirection];
            param.forwardDirection = diretionMap[dto.forwardDirection];
            param.rightDirection = diretionMap[dto.rightDirection];
            param.length = dto.length;
            param.width = dto.width;
            param.archHeight = dto.archHeight;
            if (dto.mpj1 != null)
                param.mpj1 = fromVec3dto(dto.mpj1);
            if (dto.mpj5 != null)
                param.mpj5 = fromVec3dto(dto.mpj5);
            if (dto.boh != null)
                param.boh = fromVec3dto(dto.boh);
            if (dto.mh != null)
                param.mh = fromVec3dto(dto.mh);
            if (dto.lh != null)
                param.lh = fromVec3dto(dto.lh);
            if (dto.arch != null)
                param.arch = fromVec3dto(dto.arch);
            if (dto.pcaAxisLocal != null)
                param.pcaAxisLocal = fromVec3dto(dto.pcaAxisLocal);
            if (dto.farthestPointLocal != null)
                param.farthestPointLocal = fromVec3dto(dto.farthestPointLocal);
            if (dto.position != null)
                param.position = fromVec3dto(dto.position);
            if (dto.rotation != null)
                param.rotation = fromQuaternionDto(dto.rotation);
            if (dto.mhAligned != null)
                param.mhAligned = fromVec3dto(dto.mhAligned);
            return param;
        }

        public static FootScanParametersDto mapToFootScanParametersDto(FootScanParameters param)
        {
            FootScanParametersDto dto = new FootScanParametersDto();
            dto.upDirection = getImportAxisStringEnumValue(param.upDirection);
            dto.forwardDirection = getImportAxisStringEnumValue(param.forwardDirection);
            dto.rightDirection = getImportAxisStringEnumValue(param.rightDirection);
            dto.length = param.length;
            dto.width = param.width;
            dto.archHeight = param.archHeight;
            dto.mpj1 = toVec3Dto(param.mpj1);
            dto.mpj5 = toVec3Dto(param.mpj5);
            dto.boh = toVec3Dto(param.boh);
            dto.mh = toVec3Dto(param.mh);
            dto.lh = toVec3Dto(param.lh);
            dto.arch = toVec3Dto(param.arch);
            dto.pcaAxisLocal = toVec3Dto(param.pcaAxisLocal);
            dto.farthestPointLocal = toVec3Dto(param.farthestPointLocal);
            dto.position = toVec3Dto(param.position);
            dto.rotation = toQuaternionDto(param.rotation);
            dto.mhAligned = toVec3Dto(param.mhAligned);
            return dto;
        }

        public static InsoleParameters mapToInsoleParameters(InsoleParametersDto dto)
        {
            InsoleParameters param = new InsoleParameters();
            param.adjustArchHeight = dto.adjustArchHeight;
            param.adjustArchApexPosition = dto.adjustArchApexPosition;
            param.forefootPostingAngle = dto.forefootPostingAngle;
            param.shellThickness = dto.shellThickness;
            param.firstRayCutout = dto.firstRayCutout;
            param.bevelShellEdgeThickness = dto.bevelShellEdgeThickness;
            param.bevelShellEdgeDistance = dto.bevelShellEdgeDistance;
            param.lengthWidthLength = dto.lengthWidthLength;
            param.lengthWidthWidth = dto.lengthWidthWidth;
            param.heelPostingAlpha = dto.heelPostingAlpha;
            param.heelPostingWidth = dto.heelPostingWidth;
            param.heelPostingGamma = dto.heelPostingGamma;
            param.heelPostingDelta = dto.heelPostingDelta;
            param.heelPostingEnabled = dto.heelPostingEnabled;
            param.regionBasedWidthProximalPoint = dto.regionBasedWidthProximalPoint;
            param.regionBasedWidthDistalPoint = dto.regionBasedWidthDistalPoint;
            param.regionBasedWidthWidth = dto.regionBasedWidthWidth;
            param.regionBasedWidthSmoothing = dto.regionBasedWidthSmoothing;
            param.medialFlangeEnabled = dto.medialFlangeEnabled;
            param.medialFlangeHeight = dto.medialFlangeHeight;
            param.medialFlangeWidth = dto.medialFlangeWidth;
            param.medialFlangeThickness = dto.medialFlangeThickness;
            param.lateralFlangeEnabled = dto.lateralFlangeEnabled;
            param.lateralFlangeHeight = dto.lateralFlangeHeight;
            param.lateralFlangeWidth = dto.lateralFlangeWidth;
            param.lateralFlangeThickness = dto.lateralFlangeThickness;
            param.heelSeatHeight = dto.heelSeatHeight;
            param.heelSkiveEnabled = dto.heelSkiveEnabled;
            param.heelSkiveIsMedial = dto.heelSkiveIsMedial;
            param.heelSkiveDepth = dto.heelSkiveDepth;
            param.heelSkiveAngle = dto.heelSkiveAngle;
            param.heelSkiveTilt = dto.heelSkiveTilt;
            return param;
        }

        public static InsoleParametersDto mapToInsoleParametersDto(InsoleParameters param)
        {
            InsoleParametersDto dto = new InsoleParametersDto();
            dto.adjustArchHeight = param.adjustArchHeight;
            dto.adjustArchApexPosition = param.adjustArchApexPosition;
            dto.forefootPostingAngle = param.forefootPostingAngle;
            dto.shellThickness = param.shellThickness;
            dto.firstRayCutout = param.firstRayCutout;
            dto.bevelShellEdgeThickness = param.bevelShellEdgeThickness;
            dto.bevelShellEdgeDistance = param.bevelShellEdgeDistance;
            dto.lengthWidthLength = param.lengthWidthLength;
            dto.lengthWidthWidth = param.lengthWidthWidth;
            dto.heelPostingAlpha = param.heelPostingAlpha;
            dto.heelPostingWidth = param.heelPostingWidth;
            dto.heelPostingGamma = param.heelPostingGamma;
            dto.heelPostingDelta = param.heelPostingDelta;
            dto.heelPostingEnabled = param.heelPostingEnabled;
            dto.regionBasedWidthProximalPoint = param.regionBasedWidthProximalPoint;
            dto.regionBasedWidthDistalPoint = param.regionBasedWidthDistalPoint;
            dto.regionBasedWidthWidth = param.regionBasedWidthWidth;
            dto.regionBasedWidthSmoothing = param.regionBasedWidthSmoothing;
            dto.medialFlangeEnabled = param.medialFlangeEnabled;
            dto.medialFlangeHeight = param.medialFlangeHeight;
            dto.medialFlangeWidth = param.medialFlangeWidth;
            dto.medialFlangeThickness = param.medialFlangeThickness;
            dto.lateralFlangeEnabled = param.lateralFlangeEnabled;
            dto.lateralFlangeHeight = param.lateralFlangeHeight;
            dto.lateralFlangeWidth = param.lateralFlangeWidth;
            dto.lateralFlangeThickness = param.lateralFlangeThickness;
            dto.heelSeatHeight = param.heelSeatHeight;
            dto.heelSkiveEnabled = param.heelSkiveEnabled;
            dto.heelSkiveIsMedial = param.heelSkiveIsMedial;
            dto.heelSkiveDepth = param.heelSkiveDepth;
            dto.heelSkiveAngle = param.heelSkiveAngle;
            dto.heelSkiveTilt = param.heelSkiveTilt;
            return dto;
        }

        public static ExportParameters mapToExportParameters(ExportParametersDto dto)
        {
            ExportParameters param = new ExportParameters();
            param.embossedText = dto.embossedText;
            param.fileName = dto.fileName;
            param.fontSize = dto.fontSize;
            param.hasPlantarPattern = dto.hasPlantarPattern;
            param.numVertices = new Vector2(dto.numVertices.x, dto.numVertices.y);
            param.exportFormat = exportFormatMap[dto.exportFormat];
            return param;
        }
        public static ExportParametersDto mapToExportParametersDto(ExportParameters param)
        {
            ExportParametersDto dto = new ExportParametersDto();
            dto.embossedText = param.embossedText;
            dto.fileName = param.fileName;
            dto.fontSize = param.fontSize;
            dto.hasPlantarPattern = param.hasPlantarPattern;
            Vec2Dto vec2 = new Vec2Dto((int)param.numVertices.X, (int)param.numVertices.Y);
            dto.numVertices = vec2;
            dto.exportFormat = getExportFormatStringEnumValue(param.exportFormat);
            return dto;
        }

        public static MetadataDto mapToMetadataDto(Metadata param)
        {
            MetadataDto dto = new MetadataDto();
            dto.measurementId = param.measurementId;
            dto.footScanParameters = mapToFootScanParametersDto(param.footScanParameters);
            dto.insoleName = param.insoleName;
            dto.side = getFootSideStringEnumValue(param.side);
            dto.insoleParameters = mapToInsoleParametersDto(param.insoleParameters);
            dto.exportParameters = mapToExportParametersDto(param.exportParameters);
            return dto;
        }

        public static Metadata mapToMetadata(MetadataDto dto)
        {
            Metadata metadata = new Metadata();
            metadata.side = footSideMap[dto.side];
            metadata.measurementId = dto.measurementId;
            metadata.footScanParameters = mapToFootScanParameters(dto.footScanParameters);
            metadata.insoleName = dto.insoleName;
            metadata.insoleParameters = mapToInsoleParameters(dto.insoleParameters);
            metadata.exportParameters = mapToExportParameters(dto.exportParameters);
            return metadata;
        }

        public static ApplyMetadataResponseDto mapToApplyMetadataResponseDto(InsoleData insoleData)
        {
            ApplyMetadataResponseDto dto = new ApplyMetadataResponseDto();
            dto.insoleData = insoleData.insoleData;
            dto.metaData = mapToMetadataDto(insoleData.metaData);
            return dto;
        }

        public static readonly Dictionary<string, ImportAxisDirection> diretionMap = new Dictionary<string, ImportAxisDirection> {
            { "X", ImportAxisDirection.X},
            { "Y", ImportAxisDirection.Y},
            { "Z", ImportAxisDirection.Z},
            { "NegX", ImportAxisDirection.NegX},
            { "NegY", ImportAxisDirection.NegY},
            { "NegZ", ImportAxisDirection.NegZ}
        };
        
        private static readonly Dictionary<string, ExportParameters.ExportFormat> exportFormatMap = new Dictionary<string, ExportParameters.ExportFormat > {
            { "obj", ExportParameters.ExportFormat.OBJ},
            { "stl", ExportParameters.ExportFormat.STL}
        };
        
        private static readonly Dictionary<string, FootSide> footSideMap = new Dictionary<string, FootSide > {
            { "Left", FootSide.Left},
            { "Right", FootSide.Right}
        };

        private static string getImportAxisStringEnumValue(ImportAxisDirection dir)
        {
            return diretionMap.FirstOrDefault(x => x.Value == dir).Key;
        }
        
        private static string getExportFormatStringEnumValue(ExportParameters.ExportFormat format)
        {
            return exportFormatMap.FirstOrDefault(x => x.Value == format).Key;
        }
        
        private static string getFootSideStringEnumValue(FootSide side)
        {
            return footSideMap.FirstOrDefault(x => x.Value == side).Key;
        }
        
        private static Vector3 fromVec3dto(Vec3Dto dto)
        {
            return new Vector3(dto.x, dto.y, dto.z);
        }

        private static Vec3Dto toVec3Dto(Vector3 vec)
        {
            return new Vec3Dto(vec.X, vec.Y, vec.Z);
        }
        private static Vector4 fromVec4Dto(Vec4Dto dto)
        {
            return new Vector4(dto.x, dto.y, dto.z, dto.w);
        }
        private static Vec4Dto toVec4Dto(Vector4 vec)
        {
            return new Vec4Dto(vec.X, vec.Y, vec.Z, vec.W);
        }
        
        private static Quaternion fromQuaternionDto(QuaternionDto dto)
        {
            return new Quaternion(dto.x, dto.y, dto.z, dto.w);
        }
        
        private static QuaternionDto toQuaternionDto(Quaternion vec)
        {
            return new QuaternionDto(vec.X, vec.Y, vec.Z, vec.W);
        }
    }
}
