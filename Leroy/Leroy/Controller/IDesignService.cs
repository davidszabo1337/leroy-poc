﻿using System.Threading.Tasks;
using Leroy.Dto;

namespace Leroy.Controller
{
    public interface IDesignService
    {
        Task<AppVersionDto> getVersion();
        
        Task<MetadataDto> updateLandmark(MetadataDto metadataDto);
        
        Task<MetadataDto> updateInsoleSelectionAndSizing(MetadataDto metadataDto);
        
        Task<MetadataDto> updateMetadata(MetadataDto metadataDto);
        
        Task<ApplyMetadataResponseDto> applyMetadata(MetadataDto metadataDto);
        
        Task<ExportDto> exportInsoles(MetadataDto metadataDto);
    }
}