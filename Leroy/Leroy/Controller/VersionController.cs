﻿using Microsoft.AspNetCore.Mvc;

namespace Leroy.Controller

{
    [ApiController]
    [Route("leroy/design-service/api/v1/version")]
    [Produces("application/json")]
    public class VersionController : ControllerBase
    {
        
        private readonly IDesignService service;
        
        public VersionController(IDesignService service)
        {
            this.service = service;
        }
        
        [HttpGet]
        public IActionResult GetVersion()
        {
            return StatusCode(200, service.getVersion());
        }
    }
}