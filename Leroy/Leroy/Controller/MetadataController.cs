﻿using System;
using System.Threading.Tasks;
using Leroy.Dto;
using Leroy.Exceptions;
using Leroy.Utility;
using Leroy.Utility.Validators;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
namespace Leroy.Controller
{
    [ApiController]
    [Produces("application/json")]
    [EnableCors]
    public class MetadataController : ControllerBase
    {
        private readonly IDesignService service;
        private readonly IValidator validator;
        
        public MetadataController(IDesignService service, IValidator validator)
        {
            this.service = service;
            this.validator = validator;
        }
        
        [HttpPost]
        [Route("leroy/design-service/api/v1/metadata")]
        public async Task<IActionResult> UpdateMetadata([FromBody]MetadataDto metadataDto)
        {
            try
            {   
                validator.validateMetadataDto(metadataDto);
                checkOptionalMetadata(metadataDto);
                var result = await service.updateMetadata(metadataDto);
                return StatusCode(200, result);
            }
            catch (Exception e)
            {
                var error = ErrorBuilder.buildFromException(e);
                return StatusCode(error.code, error);
            }
        }
        
        [HttpPost]
        [Route("leroy/design-service/api/v1/metadata/landmark")]
        public async Task<IActionResult> UpdateLandmarks([FromBody]MetadataDto metadataDto)
        {
            try
            {   
                validator.validateMetadataDto(metadataDto);
                checkOptionalMetadata(metadataDto);
                var result = await service.updateLandmark(metadataDto);
                return StatusCode(200, result);
            }
            catch (Exception e)
            {
                var error = ErrorBuilder.buildFromException(e);
                return StatusCode(error.code, error);
            }
        }
        
        [HttpPost]
        [Route("/design-service/api/v1/metadata/insole-selection-and-sizing")]
        public async Task<IActionResult> UpdateInsoleSelectionAndSizing([FromBody]MetadataDto metadataDto)
        {
            try
            {   
                validator.validateMetadataDto(metadataDto);
                checkOptionalMetadata(metadataDto);
                var result = await service.updateInsoleSelectionAndSizing(metadataDto);
                return StatusCode(200, result);
            }
            catch (Exception e)
            {
                var error = ErrorBuilder.buildFromException(e);
                return StatusCode(error.code, error);
            }
        }
        
        [HttpPost]
        [Route("leroy/design-service/api/v1/metadata/apply")]
        public async Task<IActionResult> ApplyMetadata([FromBody]MetadataDto metadataDto)
        { 
            try
            {   
                // validator.fullyValidateMetadata(metadataDto);
                var result = await service.applyMetadata(metadataDto);
                return StatusCode(200, result);
            }
            catch (Exception e)
            {
                var error = ErrorBuilder.buildFromException(e);
                return StatusCode(error.code, error);
            }
        }

        private void checkOptionalMetadata(MetadataDto metadataDto)
        {
            if (metadataDto.exportParameters == null)
            {
                Updater.updateExportParamsToDefaults(metadataDto);
            }
            else
            {
                validator.validateExportParametersDto(metadataDto.exportParameters);
            }

            if (metadataDto.insoleParameters == null)
            {
                Updater.updateInsoleParamsToDefaults(metadataDto);
            }
            else
            {
                validator.validateInsoleParametersDto(metadataDto.insoleParameters);
            }
        }
    }
}