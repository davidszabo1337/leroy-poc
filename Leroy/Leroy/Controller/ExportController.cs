﻿using System;
using System.Threading.Tasks;
using Leroy.Dto;
using Leroy.Exceptions;
using Leroy.Utility.Validators;
using Microsoft.AspNetCore.Mvc;
namespace Leroy.Controller
{
    [ApiController]
    [Route("leroy/design-service/api/v1/export")]
    [Produces("application/json")]
    public class ExportController : ControllerBase
    {
        private readonly IDesignService service;
        private readonly IValidator validator;
        
        public ExportController(IDesignService service, IValidator validator)
        {
            this.service = service;
            this.validator = validator;
        }
        
        [HttpPost]
        public async Task<IActionResult> ExportInsole([FromBody]MetadataDto metadataDto)
        {
            try
            {   
                validator.fullyValidateMetadata(metadataDto);
                var result = await service.exportInsoles(metadataDto);
                return StatusCode(200, result);
            }
            catch (Exception e)
            {
                var error = ErrorBuilder.buildFromException(e);
                return StatusCode(error.code, error);
            }
        }
    }
}