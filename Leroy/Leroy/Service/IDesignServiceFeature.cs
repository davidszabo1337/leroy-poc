﻿using System.Threading.Tasks;
using Leroy.Dto;

namespace Leroy.Service
{
    public interface IDesignServiceFeature
    {
        Task<MetadataDto> calcLandmarkPoints(MetadataDto dto);
        
        Task<MetadataDto> selectTemplateBasedOnLandmarks(MetadataDto dto);
        
        Task<MetadataDto> getInsole(MetadataDto dto);
        
        Task<ApplyMetadataResponseDto> generateInsole(MetadataDto dto);
        
        Task<ExportDto> exportInsole(MetadataDto dto);
    }
}