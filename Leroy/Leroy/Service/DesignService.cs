﻿using System.Threading.Tasks;
using Leroy.Controller;
using Leroy.Dto;

namespace Leroy.Service
{
    public class DesignService : IDesignService
    {
        private const int majorVersion = 0;
        private const int minorVersion = 11;
        private const int bugfixVersion = 2;

        private readonly IDesignServiceFeature designServiceFeature;

        public DesignService(IDesignServiceFeature designServiceFeature)
        {
            this.designServiceFeature = designServiceFeature;
        }

        public async Task<AppVersionDto> getVersion()
        {
            return new(getVersionString());
        }

        public async Task<MetadataDto> updateLandmark(MetadataDto metadataDto)
        {
            return await designServiceFeature.calcLandmarkPoints(metadataDto);
        }

        public async Task<MetadataDto> updateInsoleSelectionAndSizing(MetadataDto metadataDto)
        {
            return await designServiceFeature.selectTemplateBasedOnLandmarks(metadataDto);
        }

        public async Task<MetadataDto> updateMetadata(MetadataDto metadataDto)
        {
            return await designServiceFeature.getInsole(metadataDto);
        }

        public async Task<ApplyMetadataResponseDto> applyMetadata(MetadataDto metadataDto)
        {
            return await designServiceFeature.generateInsole(metadataDto);
        }

        public async Task<ExportDto> exportInsoles(MetadataDto metadataDto)
        {
            return await designServiceFeature.exportInsole(metadataDto);
        }
        
        private string createVersionString(int major, int minor, int bugfix)
        {
            return major + "." + minor + "." + bugfix;
        }
        
        private string getVersionString()
        {
            return createVersionString(majorVersion, minorVersion, bugfixVersion);
        }
    }
}